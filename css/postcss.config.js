module.exports = {
  map: { inline: true },
  plugins: [
    require("postcss-custom-media"),
    require("postcss-import-ext-glob"),
    require("postcss-import"),
    require("postcss-nested"),
    // require("postcss-preset-env")({ stage: 0, preserve: false }),
    require("cssnano")({
      preset: [
        "default",
        {
          discardComments: {
            removeAll: true,
          },
        },
      ],
    }),
  ],
};
