const path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  humanize = require("humanize-string"),
  capitalize = require("capitalize"),
  replaceAll = require("string.prototype.replaceall"),
  fileExtension = require("file-extension"),
  _7z = require("7zip-min"),
  {
    getArgs,
    name,
    log,
    cleanAffix,
    info,
    warn,
    error,
    iterate,
    chalk,
  } = require("../Utilities");
replaceAll.shim();
const start = performance.now();
const base = path.resolve("./").replaceAll("\\", "/").toLowerCase();
const dist = `${base}/extract`;
const args = getArgs();

class Battlefield2 {
  static process({ debug: debug = false }) {
    let game = `${base}/games/battlefield-2`;
    const gameBasename = path.basename(game);
    if (args.hasOwnProperty("game") && args.game !== gameBasename) {
      return;
    }
    if (args.hasOwnProperty("mod")) {
      game += `/${args.mod}`;
    }
    if (args.hasOwnProperty("source")) {
      game += `/${args.source}`;
    }
    if (args.hasOwnProperty("map")) {
      game += `/${args.map}`;
    }
    console.log(name("asd"));
    let files;
    files = walker(`${game}`, {
      fs: FS,
      nofile: true,
      depthLimit: 2,
    });
    info({
      context: "Read",
      messages: [`${files.length} maps`],
    });
    for (let i = 0; i < files.length; i++) {
      const basename = path.basename(files[i].path);
      // const levels = files[i].path
      //   .replaceAll("\\", "/")
      //   .toLowerCase()
      //   .replace(`${base}/games/`, "")
      //   .replace(`.${fileExtension(files[i].path)}`, "")
      //   .split("/");
      let mode = "Conquest";
      if (basename.includes("_ctf")) {
        mode = "CTF";
      }
      if (basename.includes("_zm")) {
        mode = "Zombie";
      }
      let target = files[i].path
        .replaceAll("\\", "/")
        .replaceAll("_", "-")
        .replace(`${base}/games`, "")
        .replace(/^\/|\/$/g, "")
        .toLowerCase();
      const parentTarget = path
        .resolve(files[i].path, "..")
        .replaceAll("\\", "/")
        .replaceAll("_", "-")
        .replace(`${base}/games`, "")
        .toLowerCase();
      const mapDepth = target.split("/").length >= 4 ? true : false;
      const mapName = capitalize
        .words(humanize(cleanAffix(target.replaceAll("/", " / "))))
        .trim();
      if (parentTarget !== "" && !FS.existsSync(`${dist}/${parentTarget}`)) {
        FS.mkdirSync(`${dist}/${parentTarget}`, { recursive: true });
      }
      iterate({
        i: i + 1,
        n: files.length,
        message: `${chalk.blueBright(mapName)} ${chalk.whiteBright(
          mapDepth ? mode : "",
        )}`,
      });
      if (!FS.existsSync(`${dist}/${target}`)) {
        FS.mkdirSync(`${dist}/${target}`, { recursive: true });
      }
      if (FS.existsSync(`${files[i].path}/info`)) {
        FS.cpSync(`${files[i].path}/info`, `${dist}/${target}`, {
          recursive: true,
        });
      }
      if (FS.existsSync(`${files[i].path}/client.zip.001`)) {
        _7z.unpack(`${files[i].path}/client.zip.001`, files[i].path, (err) => {
          if (err)
            error({
              context: "7-zip",
              messages: ["failed to unpack", `${files[i].path}/client.zip.001`],
              error: err,
            });
        });
      }
      if (FS.existsSync(`${files[i].path}/client.zip`)) {
        _7z.unpack(
          `${files[i].path}/client.zip`,
          `${dist}/${target}/client`,
          (err) => {
            if (err)
              error({
                context: "7-zip",
                messages: ["failed to unpack", `${files[i].path}/client.zip`],
                error: err,
              });
          },
        );
      }
      if (FS.existsSync(`${files[i].path}/server.zip.001`)) {
        _7z.unpack(`${files[i].path}/server.zip.001`, files[i].path, (err) => {
          if (err)
            error({
              context: "7-zip",
              messages: ["failed to unpack", `${files[i].path}/server.zip.001`],
              error: err,
            });
        });
      }
      if (FS.existsSync(`${files[i].path}/server.zip`)) {
        _7z.unpack(
          `${files[i].path}/server.zip`,
          `${dist}/${target}/server`,
          (err) => {
            if (err)
              error({
                context: "7-zip",
                messages: ["failed to unpack", `${files[i].path}/server.zip`],
                error: err,
              });
          },
        );
      }
      if (FS.existsSync(`${files[i].path}/localization`)) {
        FS.cpSync(
          `${files[i].path}/localization`,
          `${dist}/${target}/localization`,
          {
            recursive: true,
          },
        );
      }
      if (debug)
        info({
          context: "Extracted",
          messages: [`${dist}/${target}`],
        });
      log.create({
        source: name(basename),
        target: target,
        prefix: "Map",
        time: start,
      });
    }
  }
}

module.exports = Battlefield2;
