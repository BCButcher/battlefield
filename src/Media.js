const child_process = require("child_process"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  chalk = require("chalk"),
  walker = require("klaw-sync"),
  replaceAll = require("string.prototype.replaceall"),
  fileExtension = require("file-extension"),
  { getArgs, name, cleanPrefix } = require("./Utilities");
replaceAll.shim();
const start = performance.now();
const base = path.resolve("./").replaceAll("\\", "/").toLowerCase();
const dist = `${base}/media/processed`;
const args = getArgs();
const DDS = path.resolve("./src/bin/nconvert.exe");
const MAGICK = path.resolve("./src/bin/convert.exe");

process.removeAllListeners("warning");

const walkerOptions = {
  fs: FS,
  nodir: true,
  depthLimit: -1,
  traverseAll: true,
  filter: (item) => {
    return ["dds", "tga"].includes(fileExtension(item.path));
  },
};

const files = walker(
  `${base}/media/raw/battlefield-1942/galactic-conquest`,
  walkerOptions
);
console.log(chalk.cyanBright("PROCESSING"), files.length, "Image(s)");
for (let i = 0; i < files.length; i++) {
  let extension = fileExtension(files[i].path.toLowerCase());
  let target = cleanPrefix(files[i].path.toLowerCase())
    .replaceAll("\\", "/")
    .replaceAll("_", "-")
    .replace(`${base}/media/raw`, "")
    .replace(/^\/|\/$/g, "");
  if (!FS.existsSync(path.dirname(`${dist}/${target}`))) {
    FS.mkdirSync(path.dirname(`${dist}/${target}`), { recursive: true });
  }
  if (extension == "dds") {
    target = target.replaceAll(".dds", ".png");
    if (!FS.existsSync(path.dirname(`${dist}/${target}`))) {
      FS.mkdirSync(path.dirname(`${dist}/${target}`), { recursive: true });
    }
    child_process
      .execSync(
        `${DDS} -overwrite -out png -o "${dist}/${target}" "${files[i].path}"`,
        {
          timeout: 10000,
          stdio: "pipe",
        }
      )
      .toString();
    console.log(chalk.cyanBright("WROTE"), target);
    child_process
      .execSync(
        `${MAGICK} "${dist}/${target}" -trim +repage "${dist}/${target}"`,
        {
          timeout: 10000,
          stdio: "pipe",
        }
      )
      .toString();
    console.log(chalk.magentaBright("TRIMMED"), target);
  }
  if (extension == "tga") {
    target = target.replaceAll(".tga", ".png");
    child_process
      .execSync(
        `${MAGICK} "${files[i].path}" -trim +repage "${dist}/${target}"`,
        {
          timeout: 10000,
          stdio: "pipe",
        }
      )
      .toString();
    console.log(chalk.cyanBright("WROTE"), target);
  }
}
