const { performance } = require("perf_hooks"),
  path = require("path"),
  FS = require("fs"),
  walker = require("klaw-sync"),
  replaceAll = require("string.prototype.replaceall"),
  slugify = require("slugify"),
  fileExtension = require("file-extension"),
  hasDeepValue = require("has-deep-value").hasDeepValue,
  config = require("../config"),
  {
    joinTextFiles,
    getLineContent,
    getFileLineContent,
    getRegexContent,
    getXMLContent,
    writeMarkdown,
    makeFolder,
    image2jpg,
    time,
    name,
    cleanPrefix,
    getArgs,
    info,
    warn,
    error,
    chalk,
  } = require("../Utilities");
replaceAll.shim();
const start = performance.now();
const walkerOptions = {
  fs: FS,
  nofile: true,
  depthLimit: 0,
  filter: function (item) {
    return path.basename(item.path) !== "localization";
  },
};
const base = path.resolve("./").toLocaleLowerCase();
const dist =
  path.resolve("../").replaceAll("\\", "/").toLocaleLowerCase() + `/content`;
const Derivatives = require("./../data/derivatives.json");
const args = getArgs();

class Battlefield2 {
  static process({ debug: debug = false }) {
    const gameBase = `${base}/extract/battlefield-2`;
    const gameBasename = path.basename(gameBase);
    if (args.hasOwnProperty("game") && args.game !== gameBasename) {
      return;
    }
    let game = Battlefield2.meta({
      type: "game",
      basename: gameBasename,
      source: `${base}/data/${gameBasename}`,
    });
    writeMarkdown({
      file: `${dist}/${gameBasename}/${config.indexFile}`,
      metadata: game,
      type: "game",
      name: gameBasename,
      base: dist,
    });
    let mods, sources, maps;
    mods = walker(gameBase, walkerOptions);
    for (let i = 0; i < mods.length; i++) {
      const modBasename = path.basename(mods[i].path);
      if (args.hasOwnProperty("mod") && args.mod !== modBasename) {
        continue;
      }
      const modLocalization = joinTextFiles({
        dir: `${base}/extract/${gameBasename}/${modBasename}/localization`,
        encoding: "ucs2",
      });
      let mod = Battlefield2.meta({
        type: "mod",
        basename: modBasename,
        source: `${base}/data/${gameBasename}/${modBasename}`,
      });
      writeMarkdown({
        file: `${dist}/${gameBasename}/${modBasename}/${config.indexFile}`,
        metadata: mod,
        type: "mod",
        name: modBasename,
        base: dist,
      });
      sources = walker(
        `${base}/extract/${gameBasename}/${modBasename}`,
        walkerOptions,
      );
      for (let n = 0; n < sources.length; n++) {
        const sourceBasename = path.basename(sources[n].path);
        if (args.hasOwnProperty("source") && args.source !== sourceBasename) {
          continue;
        }
        let source = Battlefield2.meta({
          type: "source",
          basename: sourceBasename,
          source: `${base}/data/${gameBasename}/${modBasename}/${sourceBasename}`,
        });
        writeMarkdown({
          file: `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${config.indexFile}`,
          metadata: source,
          type: "source",
          name: sourceBasename,
          base: dist,
        });
        maps = walker(
          `${base}/extract/${gameBasename}/${modBasename}/${sourceBasename}`,
          walkerOptions,
        );
        for (let m = 0; m < maps.length; m++) {
          const mapBasename = path.basename(maps[m].path);
          if (args.hasOwnProperty("map") && args.map !== mapBasename) {
            continue;
          }
          let mapTarget = cleanPrefix(mapBasename).replaceAll("_", "-");
          let map = Battlefield2.mapMetadata({
            file: mapBasename,
            self: `${base}/extract/${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename}`,
            source: sourceBasename,
            metadataFile: `${base}/data/${gameBasename}/${modBasename}/${sourceBasename}/maps.json`,
            modLocalization: modLocalization,
            debug: debug,
          });
          if (typeof map !== "object") {
            continue;
          }
          mapTarget = slugify(map.title, { lower: true, strict: true });
          makeFolder(
            `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${mapTarget}`,
          );
          if (
            source.name &&
            source.name.length > 0 &&
            !map.categories.includes(source.name)
          ) {
            map.categories.push(source.name);
          }
          this.images({
            source: `${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename}`,
            target: `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${mapTarget}`,
            debug: debug,
          });
          const derivative = Battlefield2.derivative({
            gameBasename: gameBasename,
            modBasename: modBasename,
            sourceBasename: sourceBasename,
            mapBasename: mapBasename,
            debug: debug,
          });
          if (derivative) {
            map.original = derivative;
            map.labels.push("Modified");
          }
          writeMarkdown({
            file: `${dist}/${gameBasename}/${modBasename}/${sourceBasename}/${mapTarget}/${config.indexFile}`,
            metadata: map,
            type: "map",
            name: mapBasename,
            base: dist,
          });
        }
      }
    }
  }

  static meta({
    type: type,
    basename: basename,
    source: source,
    debug: debug,
  }) {
    let metadata = {
      title: name(basename),
      name: name(basename),
      description: "",
      categories: [],
      type: type,
      layout: `${config.layoutPrefix}${type}${config.layoutSuffix}`,
    };
    if (FS.existsSync(`${source}/metadata.json`)) {
      if (debug && Metadata.hasOwnProperty(file))
        info({
          context: "Read",
          messages: [`${source.replace(base, "")}/metadata.json`],
        });
      const Metadata = require(`${source}/metadata.json`);
      if (Metadata.hasOwnProperty("title") && Metadata["title"] !== "") {
        metadata.title = Metadata["title"];
      }
      if (Metadata.hasOwnProperty("name") && Metadata["name"] !== "") {
        metadata.name = Metadata["name"];
      }
      if (
        Metadata.hasOwnProperty("description") &&
        Metadata["description"] !== ""
      ) {
        metadata.description = Metadata["description"];
      }
      if (
        Metadata.hasOwnProperty("categories") &&
        Metadata["categories"].length > 0
      ) {
        metadata.categories = Metadata["categories"];
      }
      if (Metadata.hasOwnProperty("tags") && Metadata["tags"].length > 0) {
        metadata.tags = Metadata["tags"];
      }
      if (Metadata.hasOwnProperty("origin") && Metadata["origin"].length > 0) {
        metadata.origin = Metadata["origin"];
      }
      if (
        Metadata.hasOwnProperty("derivatives") &&
        Metadata["derivatives"].length > 0
      ) {
        metadata.derivatives = Metadata["derivatives"];
      }
    }
    if (FS.existsSync(`${source}/banner.png`)) {
      image2jpg({
        input: `${source}/banner.png`,
        output: `${dist}/${source
          .replace(base, "")
          .replace("/data", "")}/banner.jpg`,
      });
      metadata.banner = true;
    }
    return metadata;
  }

  static mapMetadata({
    file: file,
    self: self,
    source: source,
    metadataFile: metadataFile,
    modLocalization: modLocalization,
    debug: debug,
  }) {
    let metadata = {
      title: name(file),
      author: "",
      description: "",
      date: "",
      categories: ["Battlefield 2"],
      tags: ["Conquest"],
      labels: [],
      map: {
        gametypes: ["Conquest"],
        sizes: [],
        source: "",
        controlpoints: [],
      },
      type: "map",
      layout: `${config.layoutPrefix}map${config.layoutSuffix}`,
    };
    if (file.includes("-ctf") || file.includes("-zm")) {
      info({
        context: "Map",
        messages: [`Added ${metadata.title} as game mode`],
      });
      return false;
    }
    if (source !== null && source !== "") {
      metadata.categories.push(name(source));
      metadata.map.source = source;
    }
    let descriptionFile = `${self}/${file.replaceAll("-", "_")}.desc`;
    if (FS.existsSync(descriptionFile)) {
      let descriptionXML = getXMLContent({ file: descriptionFile });
      if (
        hasDeepValue(descriptionXML, "map.briefing.0._") &&
        !descriptionXML.map.briefing[0]._.toLowerCase().includes(
          "map description for modders maps not localized",
        )
      ) {
        metadata.description = descriptionXML.map.briefing[0]._;
      }
      if (
        metadata.description == "" &&
        hasDeepValue(descriptionXML, "map.briefing.0.$.locid") &&
        descriptionXML.map.briefing[0].$.locid !== "" &&
        typeof modLocalization === "string" &&
        modLocalization.length > 0
      ) {
        metadata.description = getLineContent({
          source: modLocalization,
          subString: descriptionXML.map.briefing[0].$.locid,
        });
      }
    }
    if (FS.existsSync(`${self}-ctf`)) {
      metadata.map.gametypes.push("CTF");
      metadata.tags.push("CTF");
    }
    if (FS.existsSync(`${self}-zm`)) {
      metadata.map.gametypes.push("Zombie");
      metadata.tags.push("Zombie");
    }
    if (/<maptype.*players="64"/gm.test(descriptionFile)) {
      metadata.map.sizes.push(64);
    }
    if (/<maptype.*players="32"/gm.test(descriptionFile)) {
      metadata.map.sizes.push(32);
    }
    if (/<maptype.*players="16"/gm.test(descriptionFile)) {
      metadata.map.sizes.push(16);
    }

    /* Map control points */
    /* for (let x = 0; x < metadata.map.gametypes.length; x++) {
      if (
        FS.existsSync(`${self}/${metadata.map.gametypes[x]}/ControlPoints.con`)
      ) {
        let spawns = FS.readFileSync(
          `${self}/${metadata.map.gametypes[x]}/ControlPoints.con`,
          "utf8"
        );
        const controlPointsRegex =
          /Object.create (?<name>.*)[\s\S]*?Object.absolutePosition (?<position>.*)/gm;
        let controlPoints = [];
        for (const match of spawns.matchAll(controlPointsRegex)) {
          let controlPointsPositions = match.groups.position.split("/");
          controlPointsPositions = {
            x: controlPointsPositions[0],
            y: controlPointsPositions[1],
            z: controlPointsPositions[2],
          };
          controlPoints.push({
            name: startCase(match.groups.name),
            id: match.groups.name,
            position: controlPointsPositions,
          });
        }
        metadata.map.controlpoints = controlPoints;
      }
    } */

    /* Merge external metadata */
    if (FS.existsSync(metadataFile)) {
      const Metadata = require(metadataFile);
      if (debug && Metadata.hasOwnProperty(file))
        info({
          context: "Read",
          messages: [`${metadataFile.replace(base, "")}`],
        });
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("title") &&
        Metadata[file].title.length > 0
      ) {
        metadata.title = Metadata[file].title;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("description") &&
        Metadata[file].description.length > 0
      ) {
        metadata.description = Metadata[file].description;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("labels") &&
        Metadata[file].labels.length > 0
      ) {
        metadata.labels = Metadata[file].labels;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("created") &&
        Metadata[file].created.length > 0
      ) {
        metadata.date = Metadata[file].created;
        metadata.map.created = Metadata[file].created;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("creator") &&
        Metadata[file].creator.length > 0
      ) {
        metadata.author = Metadata[file].creator;
        metadata.map.creator = Metadata[file].creator;
      }
      if (
        Metadata.hasOwnProperty(file) &&
        Metadata[file].hasOwnProperty("origin") &&
        Metadata[file].origin.length > 0
      ) {
        metadata.origin = Metadata[file].origin;
      }
      if (debug)
        info({
          context: "Built",
          messages: ["Metadata"],
          time: start,
        });
    }
    return metadata;
  }

  static images({ source: source, target: target, debug: debug }) {
    const sourceBase = `${base}/extract/${source}`;
    let imageFiles = [
      `${sourceBase}/gpm_cq_16_menumap.png`,
      `${sourceBase}/gpm_cq_32_menumap.png`,
      `${sourceBase}/gpm_cq_64_menumap.png`,
      `${sourceBase}/favoritemap.png`,
      `${sourceBase}/loadmap.png`,
      `${base}/data/${source}.png`,
    ];
    let count = 0;
    if (imageFiles.length > 0) {
      for (let i = 0; i < imageFiles.length; i++) {
        if (!FS.existsSync(`${imageFiles[i]}`)) {
          continue;
        }
        let basename = path
          .basename(imageFiles[i], `.${fileExtension(imageFiles[i])}`)
          .toLowerCase();
        let output = `${basename}.jpg`.replace("_menumap", "");
        if (
          basename == "gpm_cq_16_menumap" ||
          basename == "gpm_cq_32_menumap" ||
          basename == "gpm_cq_64_menumap"
        ) {
          output = `${target}/map.jpg`;
        }
        if (basename == "loadmap" || basename == "favoritemap") {
          output = `${target}/load.jpg`;
        }
        if (basename == path.basename(cleanPrefix(source))) {
          output = `${target}/banner.jpg`;
        }
        if (debug)
          info({
            context: "Read",
            messages: [`${imageFiles[i]}`.replace(base, "")],
          });
        image2jpg({
          input: path.resolve(`${imageFiles[i]}`),
          output: output,
        });
        if (debug)
          info({
            context: "Wrote",
            messages: [`/content${output.replace(dist, "")}`],
          });
        count++;
      }
    }
    if (debug)
      info({
        context: "Built",
        messages: [`${count} image(s)`],
        time: start,
      });
    return;
  }

  static derivative({
    gameBasename: gameBasename,
    modBasename: modBasename,
    sourceBasename: sourceBasename,
    mapBasename: mapBasename,
    debug: debug,
  }) {
    let derivatives = Derivatives;
    const index = derivatives.findIndex(
      (derivative) =>
        derivative === `${gameBasename}/${modBasename}/${sourceBasename}`,
    );
    if (index > -1) {
      derivatives = derivatives.slice(0, index);
      derivatives = derivatives.filter(
        (e) =>
          e !==
          `${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename}`,
      );
    }
    let derivation = false;
    for (let i = 0; i < derivatives.length; i++) {
      if (FS.existsSync(`${base}/extract/${derivatives[i]}/${mapBasename}`)) {
        derivation = `/${derivatives[i]}/${mapBasename}`;
        break;
      }
    }
    if (debug && derivation)
      info({
        context: "Derivative",
        messages: `${gameBasename}/${modBasename}/${sourceBasename}/${mapBasename} of ${derivation}`,
        time: start,
      });
    return derivation;
  }
}

module.exports = Battlefield2;
