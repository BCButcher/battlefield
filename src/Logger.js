const { performance } = require("perf_hooks"),
  stripAnsi = require("strip-ansi"),
  chalk = require("chalk"),
  log4js = require("log4js"),
  typeCheck = require("type-check").typeCheck;

log4js.configure({
  appenders: {
    file: {
      type: "dateFile",
      filename: "./logs/task.log",
      encoding: "utf-8",
      pattern: "yyyy-MM-dd_hh-mm-ss",
      keepFileExt: true,
      alwaysIncludePattern: true,
      layout: {
        type: "pattern",
        pattern: "[%d{yyyy-MM-dd hh:mm:ss}] [%p] %m",
      },
    },
    console: {
      type: "console",
      layout: {
        type: "pattern",
        pattern: "%d{yyyy-MM-dd hh:mm:ss} %[[%p]%] %m",
      },
    },
    debug: {
      type: "console",
      layout: {
        type: "pattern",
        pattern: chalk`[%d] %[[%p]%] %m {gray (%f:%l:%o)}`,
      },
    },
    trace: {
      type: "console",
      layout: { type: "pattern", pattern: `[%d] %[[%p]%] %m\n%[%s%]` },
    },
  },
  categories: {
    default: { appenders: ["console", "file"], level: "trace" },
    file: { appenders: ["file"], level: "trace" },
    console: {
      appenders: ["console"],
      level: "trace",
    },
    debug: {
      appenders: ["debug"],
      level: "trace",
      enableCallStack: true,
    },
    trace: {
      appenders: ["trace"],
      level: "trace",
      enableCallStack: true,
    },
  },
});
const fileLog = log4js.getLogger("file");
const consoleLog = log4js.getLogger("console");
const debuggerLog = log4js.getLogger("debug");
const stackerLog = log4js.getLogger("trace");

class Logger {
  /**
   * Formatted indicator of what file was created where
   * @param {string} source Filename or path to original
   * @param {string} target Filename or path to target location
   * @param {string} prefix Operation prefix, optional
   * @returns {string} Color-formatted status
   */
  static create({ source = source, target = target, prefix = "" }) {
    let result = chalk`{cyanBright ${prefix}} {whiteBright ${source}} => {magenta ${target}}`;
    consoleLog.info(result);
    fileLog.info(stripAnsi(result));
  }

  /**
   * Formatted info-indicator
   * @param {string} context Source of info
   * @param {array} messages Array of explanatory messages
   * @param {string} addendum Additional information, optional
   * @param {number} time Processing end-time in milliseconds, optional
   * @param {string} color Chalk color-modifier, optional
   */
  static info({
    context: context,
    messages: messages = [],
    time = 0,
    color = "cyanBright",
  }) {
    let result = chalk`{${color} ${context}}`;
    if (messages.length >= 1) {
      result += ` ${chalk.whiteBright(messages[0])}`;
      for (let i = 1; i < messages.length; i++) {
        result += messages.length > 0 ? ", " + messages.slice(1).join(" ") : "";
      }
    }
    if (time) {
      result += ` ${time > 0 ? "- " + Logger.time(time) : ""}`;
    }
    consoleLog.info(result);
    fileLog.warn(stripAnsi(result));
  }

  /**
   * Formatted warning-indicator
   * @param {string} context Source of issue
   * @param {array} messages Array of explanatory messages
   */
  static warn({ context: context, messages: messages = [] }) {
    let result = chalk`{yellowBright ${context}}`;
    if (messages.length >= 1) {
      result += ` ${chalk.whiteBright(messages[0])}`;
    }
    if (messages.length > 1) {
      for (let i = i; i < messages.length; i++) {
        result += messages.length > 0 ? ", " + messages.join(" ") : "";
      }
    }
    consoleLog.warn(result);
    fileLog.warn(stripAnsi(result));
  }

  /**
   * Formatted error-indicator
   * @param {string} context Source of issue
   * @param {array} messages Array of explanatory messages
   * @param {Error} error Error-transport
   */
  static error({ context: context, messages = [], error = "" }) {
    let result = chalk`{redBright ${context}}`;
    if (messages.length >= 1) {
      result += ` ${chalk.whiteBright(messages[0])}`;
    }
    if (messages.length > 1) {
      for (let i = i; i < messages.length; i++) {
        result += messages.length > 0 ? ", " + messages.join(" ") : "";
      }
    }
    consoleLog.error(result);
    fileLog.error(stripAnsi(result));
    if (error) {
      console.error(new Error(error));
      fileLog.error(error);
    }
  }

  /**
   * Display current stage of iteration
   * @param {number} i Item
   * @param {number} n Total
   * @param {string} message Information
   */
  static iterate({ i = i, n = n, message = message }) {
    let result = chalk`[{whiteBright ${i}/${n}}] {blueBright ${message}}`;
    consoleLog.info(result);
    fileLog.info(stripAnsi(result));
  }

  /**
   * Formatted indicator of time
   * @param {number} start Millisecond timestamp to subtract from current timestamp
   * @returns {string} Color-formatted status
   */
  static time(start) {
    return chalk`{white ${Logger.msToTime(performance.now() - start)}}`;
  }

  /**
   * Convert milliseconds to human readable time
   * @param {Number} millisec Float containing milliseconds
   *
   * @see https://stackoverflow.com/a/32180863
   */
  static msToTime(millisec) {
    var milliseconds = millisec.toFixed(2);
    var seconds = (millisec / 1000).toFixed(1);
    var minutes = (millisec / (1000 * 60)).toFixed(1);
    var hours = (millisec / (1000 * 60 * 60)).toFixed(1);
    var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);
    if (seconds <= 0) {
      return milliseconds + " ms";
    } else if (seconds < 60) {
      return seconds + " sec";
    } else if (minutes < 60) {
      return minutes + " min";
    } else if (hours < 24) {
      return hours + " hours";
    } else {
      return days + " days";
    }
  }

  /**
   * String representation of object
   * @param {object} obj Object to inspect
   */
  static inspect(obj) {
    console.log(require("util").inspect(obj, { colors: true, depth: null }));
  }

  /**
   * Shut down logger gracefully
   */
  static shutdown() {
    log4js.shutdown();
  }
}

module.exports = Logger;
