# [Battlefield 1942 and 2](https://battlefield.pages.dev/)

Information, documentation, and maps for different mods made for Battlefield 1942 and 2. All content is ephemeral, and based on the maps included in the mods and metadata gathered across the web.

## TODO

- [x] ! Title defined in metadata causes duplicate in generation
- [x] lexiconAll.dat for referenced map descriptions
  - [ ] If not enclosed in quotes
  - [x] CONSTANTS from init.con
- [x] /Mods/GCMOD/Init.con for mod description
  - Requires cleanup
  - And potential URL
- [/] /extract/battlefield-1942/\*_/_/SoldierSpawns.con for control points
  - `Object.create NAME`
  - `Object.absolutePosition X/Y/Z`
  - Relative to the square `GeometryTemplate.worldSize 1024`
    - From `../Init/Terrain.con`
- [ ] /extract/battlefield-2/\*_/_/GamePlayObjects.con for control points
  - Eg., `/pirates-2/original/black_beards_atol/server/GameModes/gpm_coop/16/GamePlayObjects.con`
  - Per size - 16, 32, 64 - eg. `Object.create CP_BBA_16_West`
    - Indented, wrapped in `if v_arg1 == host` ... `endIf`
  - Lookup pure name from `bfp2english.utxt`

## Status

Currently supports Mods, Sources, and Maps for navigation and display, but not further docs about each mod. Build-processes are under `/src/generator`, and constructed for the general case of 1942-maps, but extendable to specific cases.

## Usage

### Structure

Folder names are normalized/slugified for readability and filesystem compatibility, strictly preferring dashes, `-`, over underscores, `_`, in paths.

- **Games** are located in `/games/GAME`
- **Mods** are located in `/games/GAME/MOD`
- **Sources** are located in `/games/GAME/MOD/SOURCE`
  - A Source is typically "original" if all maps have a single origin, otherwise a natural grouping is "2011-mappack" for maps released together in 2011
- **Maps** are located in `/games/GAME/MOD/SOURCE/MAP`
  - The maps themselves are the original files as shipped with the mod, and the newest version is always used, located in the source-folder from where it originated

#### Game- and mod-specific usage

- **Battlefield 1942** maps are self-contained, `.rfa`-files which contain all necessary information
- **Battlefield 2** maps _may_ rely on information from the mod-folder, and as such the following must be included in `/games/battlefield-2/MOD`:
  - `mod.desc`
  - `localization/english/*.utxt`

#### Metadata

The structure of the source material can be complimented by a mirrored folder-structure under `/data`. When Mods and Sources are processed, files named `metadata.json` in these folders will override information inferred from source material. Source-folders can have files named `maps.json` to override map-information.

- Each Game -- `/data/GAME` -- should have a `banner.png` in its folder
- Each Mod -- `/data/GAME/MOD` -- should have a `banner.png` in its folder
- Each Mod should in `/data/GAME/MOD/metadata.json` use the attributes:
  - `title` to define a title that would not be derived from humanizing the folder-name, used for display
  - `name` to define a name other than the title, used for organizing
  - `description` to describe the mod, as closely as possible to its original, and may contain Markdown-formatting
  - `categories` to relate this mod to its relevant game, eg. `["Battlefield 1942"]`
  - `origin`, a link to the homepage of the mod, if it still exists, alternatively using the [WayBack Machine](https://web.archive.org/)

```json
{
  "title": "Battlefield Pirates",
  "description": "Battlefield Pirates ...",
  "categories": ["Battlefield 1942"]
}
```

- Each Source should in `/data/GAME/MOD/SOURCE/metadata.json` use the attributes:
  - `title` to define a title that would not be derived from humanizing the folder-name, used for display
  - `name` to define a name other than the title, used for organizing
  - `description` to describe the source, as closely as possible to its original, and may contain Markdown-formatting
  - `categories` to relate this source to its relevant mod, eg. `["Pirates"]`
  - `origin`, a link to the source of the release of the maps, if it still exists, alternatively using the [WayBack Machine](https://web.archive.org/)

```json
{
  "title": "BFP1 Original Maps",
  "description": "Battlefield Pirates ...",
  "categories": ["Pirates"],
  "origin": "https://web.archive.org/web/20090225072254/http://bfpirates.com:80/wiki/index.php/BFP"
}
```

- Each Map should in `/data/GAME/MOD/SOURCE/maps.json` use the attributes:
  - `created` to define the date the map was **first** released, in the ISO 8601 format YYYY-MM-DD
  - `creator` to define who created the original map
  - `title` to define a title that would not be derived from humanizing the folder-name
  - `description` to describe the map, as closely as possible to its original, and may contain Markdown-formatting
  - `labels` to relate this map to other maps, excluding Mod, Source or Game Type such as Conquest, CTF, TDM, etc., eg. `["Release 1"]`
  - `origin`, a link to the source of this information, if it still exists, alternatively using the [WayBack Machine](https://web.archive.org/)
  - **Note**: This file contains an object with child-objects, where each key is the normalized/slugified name of the map, eg. "bfp_sanctuary"

```json
{
  ...
  "bfp_frylar": {
    "created": "2005-11-01",
    "creator": "Stealth",
    "labels": ["Release 1"],
    "description": "Frylar was released as a BFP 1.0 map ...",
    "origin": "https://web.archive.org/web/20080327022047/http://www.bfpirates.com/wiki/index.php/Frylar"
  },
  ...
}
```

### Custom maps and derivatives

Custom maps are those that did not ship as an aggregated source, such as a map pack or comprehensive modification of a mod. They should be placed in their respective folder in `/games/MOD/custom/MAP`, and metadata added to `/data/GAME/MOD/custom/maps.json`.

Some mods ship modified versions of vanilla or other maps. To correctly identify derivatives, utilize the `derivatives.json`-file in `/data`, consisting of an array of sources in order of importance or appearance. When declared, a source's map is labelled as "Modified" and linked to its original. Names must include the folder-name of the mod _and_ source, eg:

```json
[
  "1942/original",
  "road_to_rome/original",
  "secret_weapons_of_ww2/original",
  "desert_combat/original",
  "desert_combat/final",
  "desert_combat/realism"
]
```

### Processing

Prerequisites are [Node v17 and NPM v8](https://nodejs.dev/), as well as [Hugo v0.99](https://gohugo.io/), or above.

1. Run `npm install`
2. Run `npm run extract` to extract the contents of all maps with `rfaUnpack` into `/extract`
3. Run `npm run generate` to process all mods, sources, and maps, and place the resulting Markdown-files into `/content`
4. Run `hugo server` for local development
5. Run `hugo` to build content for publishing into `/public`

Add `-- --debug`, noting the amount of parameter-separators, to get extra console-output.

#### Arguments

Include the following parameters to filter and limit processing:

- **Game**: `--game="battlefield-1942"`, to only process maps in `/Extract/battlefield-1942`
- **Mod**: `--mod="pirates"`, to only process maps in `/Extract/GAME/pirates`
- **Source**: `--source="original"`, to only process maps in `/Extract/GAME/MOD/original`
- **Map**: `--map="frylar"`, to only process the map in `/Extract/GAME/MOD/SOURCE/frylar`. This parameter searches by substring rather than exact match.

**Note**: Processing is exclusionary, from top to bottom, and thus adding parameters will filter how much data is processed and improve performance.

##### Examples

Extract all Battlefield 1942 maps:

`yarn workspace 42and2-src extract --game="battlefield-1942"`

Generate all Battlefield 1942 maps:

`yarn workspace 42and2-src generate --game="battlefield-1942"`

Generate all Desert Combat maps for Battlefield 1942:

`yarn workspace 42and2-src generate --game="battlefield-1942" --mod="desert-combat"`

Generate all Desert Combat Final maps for Battlefield 1942:

`yarn workspace 42and2-src generate --game="battlefield-1942" --mod="desert-combat" --source="final"`

Generate any maps with "bulge" in the name for Battlefield 1942:

`yarn workspace 42and2-src generate --game="battlefield-1942" --map="bulge"`

Generate any maps with "bulge" in the name, specifically for Desert Combat Final, for Battlefield 1942:

`yarn workspace 42and2-src generate --game="battlefield-1942" --mod="desert-combat" --source="final" --map="bulge"`
`yarn workspace 42and2-src generate --game="battlefield-1942" --mod="1942" --source="original" --map="aberdeen"`

Generate any maps with "frylar" in the name, specifically for Pirates 2 Original, for Battlefield 2:

`yarn workspace 42and2-src generate --game="battlefield-2" --mod="pirates-2" --source="original" --map="frylar2"`
