---
title: Cliffhanger
author: Muad'Dib
description: The Peglegs assault a heavily fortified Skullclan fort. Good luck mateys.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: Castle
      id: Castle
      position:
        x: '625.38'
        'y': '90.04'
        z: '336.56'
    - name: Town
      id: Town
      position:
        x: '624.33'
        'y': '68.73'
        z: '455.23'
    - name: Fortifications East
      id: Fortifications_east
      position:
        x: '744.51'
        'y': '77.86'
        z: '403.27'
    - name: Fortifications West
      id: Fortifications_west
      position:
        x: '506.63'
        'y': '77.85'
        z: '401.78'
    - name: Blueshipp
      id: Blueshipp
      position:
        x: '773.47'
        'y': '46.90'
        z: '747.54'
    - name: Island
      id: Island
      position:
        x: '215.26'
        'y': '42.87'
        z: '845.36'
    - name: The Beach
      id: The_Beach
      position:
        x: '627.50'
        'y': '44.37'
        z: '519.49'
  created: '2007-09-21'
  creator: Muad'Dib
---

