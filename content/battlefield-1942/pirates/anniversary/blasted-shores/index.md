---
title: Blasted Shores
author: Brutus
description: Get the booty, and no excuses.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Skullclan Base
      id: Skullclan_Base
      position:
        x: '498.33'
        'y': '106.28'
        z: '336.30'
    - name: Peg Base
      id: Peg_Base
      position:
        x: '493.57'
        'y': '107.14'
        z: '663.47'
    - name: Peg Tower
      id: Peg_Tower
      position:
        x: '494.00'
        'y': '146.22'
        z: '648.45'
    - name: Skull Tower
      id: Skull_Tower
      position:
        x: '497.98'
        'y': '146.32'
        z: '350.59'
    - name: Red Town
      id: Red_Town
      position:
        x: '503.99'
        'y': '101.83'
        z: '451.94'
    - name: Blue Town
      id: Blue_Town
      position:
        x: '499.49'
        'y': '101.85'
        z: '555.74'
  created: '2007-09-21'
  creator: Brutus
---

