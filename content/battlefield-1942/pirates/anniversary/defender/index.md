---
title: Defender
author: Archimonde
description: Assault the isle and defeat ye enemy! Holding the Middle Flag may reward ye...
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: The Keep
      id: The_Keep
      position:
        x: '482.15'
        'y': '57.21'
        z: '465.44'
    - name: East Wall
      id: East_Wall
      position:
        x: '525.20'
        'y': '54.43'
        z: '465.15'
    - name: North Wall
      id: North_Wall
      position:
        x: '483.94'
        'y': '54.42'
        z: '505.05'
    - name: West Wall
      id: West_Wall
      position:
        x: '442.48'
        'y': '54.49'
        z: '465.38'
    - name: South Wall
      id: South_Wall
      position:
        x: '480.78'
        'y': '54.46'
        z: '420.62'
    - name: Red Base
      id: Red_Base
      position:
        x: '234.10'
        'y': '50.97'
        z: '277.92'
    - name: Blue Base
      id: Blue_Base
      position:
        x: '676.70'
        'y': '50.93'
        z: '793.74'
  created: '2007-09-21'
  creator: Archimonde
---

