---
title: Isle Of Woe
author: Brutus
description: >-
  The pirates that once used this island as a refuge have left it forever after
  their captain disappeared under very strange conditions. Now, referred to it
  only as the haunted Isle of Woe, it is rumored the superstitious pirates
  abandoned their treasure and stores when hastily evacuating their once safe
  haven.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '44.49'
        'y': '75.81'
        z: '513.79'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '958.93'
        'y': '75.73'
        z: '514.47'
    - name: Breakers
      id: Breakers
      position:
        x: '516.68'
        'y': '111.84'
        z: '414.28'
    - name: Fort
      id: Fort
      position:
        x: '509.93'
        'y': '99.37'
        z: '507.96'
    - name: Tower Point
      id: Tower_Point
      position:
        x: '510.47'
        'y': '99.10'
        z: '597.39'
  created: '2007-09-21'
  creator: Brutus
---

