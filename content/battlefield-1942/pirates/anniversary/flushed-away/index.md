---
title: Flushed Away
author: Muad'Dib
description: >-
  After the British were flushed away from this island by a huge flood the
  Peglegs and Skullclan come to take it.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: Castle
      id: Castle
      position:
        x: '400.45'
        'y': '106.25'
        z: '496.17'
    - name: South Town
      id: South_town
      position:
        x: '452.34'
        'y': '84.62'
        z: '395.82'
    - name: North Town
      id: North_town
      position:
        x: '442.22'
        'y': '82.38'
        z: '576.91'
    - name: Blue Ship
      id: Blue_ship
      position:
        x: '837.89'
        'y': '85.06'
        z: '778.82'
    - name: Red Ship
      id: Red_ship
      position:
        x: '837.89'
        'y': '86.95'
        z: '245.18'
  created: '2007-09-21'
  creator: Muad'Dib
---

