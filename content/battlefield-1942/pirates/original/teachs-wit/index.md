---
title: Teachs Wit
author: ''
description: >-
  Blackbeard (aka Edward Teach) was a clever and cold-blooded killer. In his
  honor, this ship-wrecking iceberg has been dubbed (as cold as) Teach's Wit.
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '1100.14'
        'y': '165.10'
        z: '160.58'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '1126.55'
        'y': '165.10'
        z: '1751.52'
    - name: Lost Booty
      id: Lost_Booty
      position:
        x: '1136.12'
        'y': '330.94'
        z: '955.78'
    - name: Boneyard
      id: Boneyard
      position:
        x: '1085.55'
        'y': '242.62'
        z: '963.70'
  created: '2005-11-01'
---

