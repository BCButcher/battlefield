---
title: Cut Throat Creek
author: ''
description: >-
  Cap all ye flags if ye can be managin' to dodge all manner of mortar,
  explosive device or hurled blunt objects
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Skull Clan Fortress
      id: SkullClan_Fortress
      position:
        x: '635.06'
        'y': '166.88'
        z: '550.85'
    - name: Peg Leg Fortress
      id: PegLeg_Fortress
      position:
        x: '446.67'
        'y': '166.99'
        z: '553.43'
    - name: North Bridge
      id: North_Bridge
      position:
        x: '538.38'
        'y': '139.73'
        z: '593.11'
    - name: Pegleg Bank
      id: Pegleg_Bank
      position:
        x: '512.28'
        'y': '139.57'
        z: '556.39'
    - name: South Bridge
      id: South_Bridge
      position:
        x: '537.40'
        'y': '140.65'
        z: '514.18'
    - name: River Bed
      id: River_Bed
      position:
        x: '542.53'
        'y': '128.08'
        z: '555.45'
  created: '2004-07-13'
---

