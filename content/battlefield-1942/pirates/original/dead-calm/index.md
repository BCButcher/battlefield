---
title: Dead Calm
author: ''
description: >-
  Wouldn't you know it? Right in the middle of the biggest battle of their lives
  and the wind completely dies! Well, instead of blasting each other's ships to
  smithereens, they agree to do the 'honorable' thing and fight it out
  pirate-to-pirate, but that's about as far as their honor goes..........There
  is only one kit, so it doesn't matter which class you select. Additionally, in
  this map you can jump onto the island from any height and not take damage.
  Just don't hit the chests!
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Skull Clan Galleon
      id: SkullClan_Galleon
      position:
        x: '512.00'
        'y': '47.02'
        z: '531.00'
    - name: Peg Leg Galleon
      id: PegLeg_Galleon
      position:
        x: '512.00'
        'y': '47.04'
        z: '493.00'
    - name: Treasure
      id: Treasure
      position:
        x: '518.26'
        'y': '40.43'
        z: '512.70'
  created: '2004-07-13'
---

