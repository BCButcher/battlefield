---
title: Haunted Cove
author: ''
description: The SkullClan returns to haunt Scurvy Cove...
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Red Galleon
      id: Red_Galleon
      position:
        x: '1564.56'
        'y': '69.54'
        z: '1456.97'
    - name: Church
      id: Church
      position:
        x: '1245.47'
        'y': '56.54'
        z: '1566.73'
    - name: Scurvy Tower
      id: Scurvy_Tower
      position:
        x: '1145.27'
        'y': '120.49'
        z: '1570.96'
    - name: Blue Cove
      id: blue_cove
      position:
        x: '1205.38'
        'y': '99.06'
        z: '1472.75'
  created: '2005-11-01'
---

