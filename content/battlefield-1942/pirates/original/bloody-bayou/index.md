---
title: Bloody Bayou
author: ''
description: Get that old hermit's gold!
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '520.50'
        'y': '76.97'
        z: '673.20'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '512.06'
        'y': '76.35'
        z: '472.94'
  created: '2005-11-01'
---

