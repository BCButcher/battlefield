---
title: Marooned
author: Brutus
description: >-
  During the initial BFP 1.0 testing phase, Marooned originally had small
  vessels. However, people would grab the flag and jump in a boat and sail back
  to home. If someone else got the other flag, they'd jump in a boat and sail
  away from the island causing a stand still. Anyone who didn't get a boat would
  have to wait on the shore while everyone else chased down the carriers. The
  vessels they were using weren't equipped with cannons, so they only way they
  could get the flag back was ramming the boat, which would sink both ships.
  Players would then have to wait until the flag respawned back to it's original
  position.
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - CTF
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '512.11'
        'y': '76.20'
        z: '397.11'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '510.78'
        'y': '76.20'
        z: '570.26'
  created: '2005-11-01'
  creator: Brutus
origin: >-
  https://web.archive.org/web/20080327013922/http://www.bfpirates.com/wiki/index.php/Marooned
---

