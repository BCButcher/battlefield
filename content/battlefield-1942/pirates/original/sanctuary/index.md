---
title: Sanctuary
author: ''
description: >-
  The Peglegs have blockaded a sacred Skullclan refuge. The first Pegleg siege
  was repulsed but at great cost to the Skullclan. The Skullclan have strained
  the last of their dwindled resources to fortify their defenses. Further
  repairs and resupply will be difficult! Can the Peglegs overcome the maniacal
  defenders of Skullclan Sanctuary?
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Crypt
      id: Crypt
      position:
        x: '725.78'
        'y': '188.21'
        z: '1578.50'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '1008.50'
        'y': '193.91'
        z: '671.71'
    - name: Flotsam
      id: Flotsam
      position:
        x: '980.14'
        'y': '186.68'
        z: '1156.16'
    - name: Shore Batteries
      id: Shore_Batteries
      position:
        x: '997.85'
        'y': '220.09'
        z: '1284.10'
    - name: Storehouse
      id: Storehouse
      position:
        x: '1007.73'
        'y': '242.14'
        z: '1423.26'
    - name: Gallows
      id: Gallows
      position:
        x: '1015.77'
        'y': '252.17'
        z: '1511.65'
    - name: Reliquary
      id: Reliquary
      position:
        x: '991.30'
        'y': '316.64'
        z: '1561.68'
  created: '2005-11-01'
---

