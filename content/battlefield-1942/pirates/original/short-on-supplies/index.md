---
title: Short On Supplies
author: ''
description: >-
  Both crews came for supplies. Nobody even considered splitting the last keg of
  rum...
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '372.78'
        'y': '101.30'
        z: '692.98'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '363.38'
        'y': '101.46'
        z: '597.00'
    - name: Bridge
      id: Bridge
      position:
        x: '368.31'
        'y': '84.93'
        z: '643.93'
  created: '2005-11-01'
---

