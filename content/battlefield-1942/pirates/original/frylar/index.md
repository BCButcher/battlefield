---
title: Frylar
author: Stealth
description: >-
  Frylar was released as a BFP 1.0 map designed by Stealth. The map is a similar
  style of Shiver Me Timbers, but the flags are closer in proximity. Thus, the
  map is more chaotic. There are a total 5 flags: 2 Uncappables, 3 Cappables. If
  you capture the two outer flags, a bleed with ensue. If you capture the center
  flag, a bleed will ensue. This map is one of the few maps in BFP1 that you can
  have two bleeds going on simultaneously.
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Skullclan Base
      id: Skullclan_Base
      position:
        x: '340.42'
        'y': '88.83'
        z: '367.21'
    - name: Pegleg Base
      id: Pegleg_Base
      position:
        x: '470.98'
        'y': '76.20'
        z: '319.32'
    - name: The Huts
      id: The_Huts
      position:
        x: '438.57'
        'y': '81.97'
        z: '394.80'
    - name: The Far Tower
      id: The_Far_Tower
      position:
        x: '376.04'
        'y': '94.44'
        z: '273.86'
    - name: The Surrounded
      id: The_Surrounded
      position:
        x: '395.00'
        'y': '85.35'
        z: '333.76'
  created: '2005-11-01'
  creator: Stealth
origin: >-
  https://web.archive.org/web/20080327022047/http://www.bfpirates.com/wiki/index.php/Frylar
---

