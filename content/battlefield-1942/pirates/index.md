---
title: Pirates
name: Pirates
description: >-
  Battlefield Pirates is a full conversion mod of Battlefield 1942. The general
  theme of the game is two rivaling clans, the Skull Clan and the Peglegs. These
  two pirate clans have been clashing at each other's throats for years and over
  the course of time, they have evolved with new weaponry to kill one another.
  Brace yourself for a fun-filled adventure on the high seas!
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
banner: true
---

