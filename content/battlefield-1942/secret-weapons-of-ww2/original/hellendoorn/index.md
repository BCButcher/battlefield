---
title: Hellendoorn
author: DICE, EA
description: ''
date: '2003-09-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - Objectives
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - Objectives
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '291.22'
        'y': '87.2655'
        z: '476.72'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '697.534'
        'y': '64.0102'
        z: '771.895'
    - name: Hellendoorn
      id: Hellendoorn
      position:
        x: '493.348'
        'y': '80.1586'
        z: '651.617'
    - name: Allied Village
      id: Allied_Village
      position:
        x: '574.492'
        'y': '59.4741'
        z: '933.41'
    - name: Axis Village
      id: Axis_Village
      position:
        x: '346.315'
        'y': '61.9465'
        z: '248.284'
    - name: Axis Airfield
      id: Axis_Airfield
      position:
        x: '669.395'
        'y': '59.673'
        z: '473.922'
    - name: Allied Airfield
      id: Allied_Airfield
      position:
        x: '322.304'
        'y': '63.3767'
        z: '821.885'
  created: '2003-09-04'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

