---
title: Essen
author: DICE, EA
description: ''
date: '2003-09-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - Objectives
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - Objectives
  size: 2048
  source: original
  controlpoints:
    - name: Allies
      id: allies
      position:
        x: '1254.53'
        'y': '40.979'
        z: '870.298'
    - name: Essen Front
      id: essen_front
      position:
        x: '609.7'
        'y': '33.0805'
        z: '880.851'
    - name: EC 1
      id: EC_1
      position:
        x: '448.006'
        'y': '33.0805'
        z: '915.838'
    - name: EC 2
      id: EC_2
      position:
        x: '527.579'
        'y': '33.0805'
        z: '821.407'
    - name: Factory
      id: factory
      position:
        x: '314.28'
        'y': '36.9496'
        z: '881.227'
  created: '2003-09-04'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

