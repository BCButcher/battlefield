---
title: Peenemunde
author: DICE, EA
description: ''
date: '2003-09-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '869.109'
        'y': '90.4289'
        z: '1064.53'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '1167.44'
        'y': '103.845'
        z: '455.403'
    - name: Roadblock
      id: roadblock
      position:
        x: '762.55'
        'y': '90.4257'
        z: '921.677'
    - name: Church
      id: church
      position:
        x: '1035.99'
        'y': '90.4289'
        z: '742.382'
    - name: Airfield
      id: airfield
      position:
        x: '774.902'
        'y': '90.4571'
        z: '502.676'
  created: '2003-09-04'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

