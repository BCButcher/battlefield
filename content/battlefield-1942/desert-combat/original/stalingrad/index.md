---
title: Stalingrad
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Original
tags:
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 416
  source: original
  controlpoints:
    - name: CONTROLPOINT Stalingrad
      id: CONTROLPOINT_stalingrad
      position:
        x: '535.6'
        'y': '38.'
        z: '326.813'
    - name: AXIS CONTROLPOINT CITY
      id: AXIS_CONTROLPOINT_CITY
      position:
        x: '548.018'
        'y': '38.055'
        z: '387.729'
    - name: RUSSIAN CONTROLPOINT CITY
      id: 'RUSSIAN_CONTROLPOINT_CITY '
      position:
        x: '506.887'
        'y': '38.5012'
        z: '270.489'
  created: '2004-02-04'
---

