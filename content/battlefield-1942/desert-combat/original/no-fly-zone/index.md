---
title: No Fly Zone
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 4096
  source: original
  controlpoints:
    - name: US CTF Base
      id: US_CTF_Base
      position:
        x: '864.19'
        'y': '51.36'
        z: '3461.37'
    - name: Iraqi CTF Base
      id: Iraqi_CTF_Base
      position:
        x: '2705.80'
        'y': '38.99'
        z: '1157.48'
---

