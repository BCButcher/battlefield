---
title: Twin Rivers
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 0
  source: realism
  controlpoints:
    - name: Terroristcamp
      id: terroristcamp
      position:
        x: '693.011'
        'y': '49.5812'
        z: '939.721'
    - name: Terroristoutpost
      id: terroristoutpost
      position:
        x: '1254.47'
        'y': '40.2'
        z: '1014.72'
    - name: Alliesbase
      id: alliesbase
      position:
        x: '1486.79'
        'y': '72.9969'
        z: '573.025'
    - name: Terroristsupplybase
      id: terroristsupplybase
      position:
        x: '474.4828'
        'y': '50.98125'
        z: '1229.716'
    - name: Ammocache
      id: ammocache
      position:
        x: '910.978'
        'y': '50.2531'
        z: '938.955'
    - name: Rpo
      id: rpo
      position:
        x: '255.344'
        'y': '40.0797'
        z: '849.293'
---

