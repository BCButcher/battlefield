---
title: Tobruk
author: Trauma Studios, DC Final Team, DC Realism Team
description: ''
date: '2005-06-18'
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: realism
  controlpoints:
    - name: AXIS BASE
      id: 'AXIS_BASE '
      position:
        x: '2546.28'
        'y': '69.6516'
        z: '817.944'
    - name: AXIS BASE CITY
      id: AXIS_BASE_CITY
      position:
        x: '2401.45'
        'y': '80.6862'
        z: '819.998'
    - name: AXIS BASE SECONDLINE Right
      id: 'AXIS_BASE_SECONDLINE_right '
      position:
        x: '2233.68'
        'y': '73.7306'
        z: '717.589'
    - name: AXIS BASE SECONDLINE Left
      id: 'AXIS_BASE_SECONDLINE_left '
      position:
        x: '2217.26'
        'y': '78.6635'
        z: '842.967'
    - name: AXIS BASE FIRSTLINE Right
      id: 'AXIS_BASE_FIRSTLINE_right '
      position:
        x: '2083.9'
        'y': '69.2736'
        z: '612.068'
    - name: AXIS BASE FIRSTLINE Middle
      id: 'AXIS_BASE_FIRSTLINE_middle '
      position:
        x: '2038.04'
        'y': '71.5807'
        z: '724.311'
    - name: AXIS BASE FIRSTLINE Left
      id: AXIS_BASE_FIRSTLINE_left
      position:
        x: '1976.1'
        'y': '80.5135'
        z: '841.519'
    - name: ALLIES BASE
      id: ALLIES_BASE
      position:
        x: '1743.65'
        'y': '67.1438'
        z: '572.867'
  created: '2005-06-18'
  creator: Trauma Studios, DC Final Team, DC Realism Team
origin: >-
  https://web.archive.org/web/20220611145835/http://www.tanelorn.us/dcr/pages/maps.htm
---

