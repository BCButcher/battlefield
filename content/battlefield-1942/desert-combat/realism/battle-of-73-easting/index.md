---
title: Battle Of 73 Easting
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: realism
  controlpoints: []
---

