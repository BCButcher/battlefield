---
title: Coastal Hammer
author: ''
description: >-
  OPERATION COASTAL HAMMER: An amphibious brigade of Iraqi mechanized infantry
  has been tasked with the invasion and re-capture of a coastal town currently
  occupied by Coalition forces. The invasion is to be launched from a staging
  area on a small off-shore island. The coalition side must hold all changeable
  control points, while the opposition must capture four to achieve enemy ticket
  bleed.
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: realism
  controlpoints: []
---

