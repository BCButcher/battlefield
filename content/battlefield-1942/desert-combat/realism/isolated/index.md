---
title: Isolated
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: realism
  controlpoints:
    - name: Iraqi Held Town
      id: Iraqi_held_town
      position:
        x: '13.02'
        'y': '76.50'
        z: '9.72'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '2022.26'
        'y': '76.35'
        z: '2016.89'
    - name: House 4
      id: House_4
      position:
        x: '906.11'
        'y': '52.41'
        z: '1030.18'
    - name: House 3
      id: House_3
      position:
        x: '905.89'
        'y': '55.33'
        z: '1126.97'
    - name: Secondary Objective
      id: Secondary_objective
      position:
        x: '777.74'
        'y': '90.08'
        z: '1099.71'
    - name: House 1
      id: House_1
      position:
        x: '1148.38'
        'y': '76.54'
        z: '1329.42'
    - name: House 2
      id: House_2
      position:
        x: '1170.12'
        'y': '94.28'
        z: '1022.78'
    - name: Amoured Patrol
      id: Amoured_patrol
      position:
        x: '24.26'
        'y': '76.58'
        z: '10.30'
---

