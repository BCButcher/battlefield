---
title: Bridge Crossing
author: ''
description: ' This is a push map so you must take the flags in order '
date: ''
categories:
  - Battlefield 1942
  - Realism
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: realism
  controlpoints:
    - name: Iraqbasetemplate
      id: iraqbasetemplate
      position:
        x: '797.79'
        'y': '62.0626'
        z: '531.45'
    - name: Coalitiontemplate
      id: coalitiontemplate
      position:
        x: '503.57'
        'y': '51.86'
        z: '1971.19'
    - name: Bridgetemplate
      id: bridgetemplate
      position:
        x: '1006.32'
        'y': '53.73'
        z: '759.54'
    - name: Villagetemplate
      id: villagetemplate
      position:
        x: '1041.47'
        'y': '53.0966'
        z: '1250.86'
---

