---
title: Wake
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
  size: 0
  source: final
  controlpoints:
    - name: The Airfield
      id: The_Airfield
      position:
        x: '1383.75'
        'y': '115.998'
        z: '775.193'
  created: '2004-02-04'
---

