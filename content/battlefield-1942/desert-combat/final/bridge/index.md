---
title: Bridge
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: final
  controlpoints:
    - name: Market
      id: market
      position:
        x: '73.63'
        'y': '80.39'
        z: '934.97'
    - name: Plaza
      id: plaza
      position:
        x: '157.65'
        'y': '77.35'
        z: '730.27'
    - name: Southpoint
      id: southpoint
      position:
        x: '103.71'
        'y': '76.92'
        z: '663.63'
    - name: Northpoint
      id: northpoint
      position:
        x: '174.61'
        'y': '80.46'
        z: '953.67'
    - name: Bridge
      id: Bridge
      position:
        x: '121.73'
        'y': '84.70'
        z: '823.43'
---

