---
title: Basrahs Edge
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 1024
  source: final
  controlpoints:
    - name: Crashspawn
      id: crashspawn
      position:
        x: '549.6031'
        'y': '1.794128'
        z: '507.3176'
    - name: Iraqspawn
      id: iraqspawn
      position:
        x: '338.6008'
        'y': '2.6'
        z: '409.9239'
    - name: Marketspawn
      id: marketspawn
      position:
        x: '420'
        'y': '2.961664'
        z: '516'
    - name: Slumspawn
      id: slumspawn
      position:
        x: '648'
        'y': '14.37597'
        z: '616'
    - name: U Sspawn
      id: USspawn
      position:
        x: '714.2375'
        'y': '25.8089'
        z: '735.9367'
---

