---
title: First Light
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 2048
  source: final
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '1024.000000'
        'y': '76.599998'
        z: '964.000000'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '1024.000000'
        'y': '72.889999'
        z: '1084.000000'
    - name: US Base
      id: US_Base
      position:
        x: '599.334717'
        'y': '70.704803'
        z: '270.215698'
---

