---
title: Al Nas Day2
author: ''
description: Welcome to Al Nas Day 2
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: final
  controlpoints:
    - name: Axis Base
      id: axis_base
      position:
        x: '712.185'
        'y': '38.9406'
        z: '333.864'
    - name: Russian Base
      id: russian_base
      position:
        x: '456.7502'
        'y': '30.7438'
        z: '139.0542'
    - name: Axis Controlpoint City
      id: axis_controlpoint_city
      position:
        x: '588.275'
        'y': '26.3458'
        z: '297.742'
    - name: Russian Controlpoint City
      id: russian_controlpoint_city
      position:
        x: '481.647'
        'y': '26.3615'
        z: '266.586'
---

