---
title: Al Nas
author: ''
description: Welcome to Al Nas Day 1
date: ''
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: final
  controlpoints:
    - name: Axis Base
      id: axis_base
      position:
        x: '712.185'
        'y': '38.9406'
        z: '333.864'
    - name: Russian Base
      id: russian_base
      position:
        x: '456.8253'
        'y': '30.71255'
        z: '139.6509'
    - name: Axis Controlpoint City
      id: axis_controlpoint_city
      position:
        x: '624.242'
        'y': '26.3771'
        z: '274.573'
    - name: Russian Controlpoint City
      id: russian_controlpoint_city
      position:
        x: '553.734'
        'y': '26.3615'
        z: '218.796'
    - name: Russian Controlpoint City 2
      id: russian_controlpoint_city2
      position:
        x: '670.521'
        'y': '32.6813'
        z: '244.748'
---

