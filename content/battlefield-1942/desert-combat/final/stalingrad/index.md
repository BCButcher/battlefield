---
title: Stalingrad
author: ''
description: ''
date: '2004-02-04'
categories:
  - Battlefield 1942
  - Final
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 416
  source: final
  controlpoints:
    - name: AXIS BASE
      id: 'AXIS_BASE '
      position:
        x: '698.411'
        'y': '37.8'
        z: '378.499'
    - name: RUSSIAN BASE
      id: RUSSIAN_BASE
      position:
        x: '555.326'
        'y': '26.1969'
        z: '190.71'
    - name: AXIS CONTROLPOINT CITY
      id: AXIS_CONTROLPOINT_CITY
      position:
        x: '535.38'
        'y': '51.299'
        z: '388.564'
    - name: RUSSIAN CONTROLPOINT CITY
      id: 'RUSSIAN_CONTROLPOINT_CITY '
      position:
        x: '523.632'
        'y': '51.299'
        z: '273.109'
    - name: CONTROLPOINT Stalingrad
      id: CONTROLPOINT_stalingrad
      position:
        x: '535.6'
        'y': '38.'
        z: '326.813'
  created: '2004-02-04'
---

