---
title: Circarpous Iv Raid
author: ''
description: Xwing VS Tiefighter Dogfighting map
date: ''
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - TDM
  size: 2048
  source: extended
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '944.80'
        'y': '12.70'
        z: '2042.07'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '1007.15'
        'y': '12.68'
        z: '2.30'
---

