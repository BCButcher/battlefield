---
title: Two Moons
author: Dnamro
description: >-
  The battle for Two Moons..there is 1 sheild generator on this planet and is a
  cricial link to the other sheilgenerators in this system and your job is to
  capture and hold that post
date: '2003-08-25'
categories:
  - Battlefield 1942
  - Extended
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 1024
  source: extended
  controlpoints:
    - name: AXIS CONTROLPOINT CITY
      id: AXIS_CONTROLPOINT_CITY
      position:
        x: '662.29'
        'y': '103.00'
        z: '502.49'
    - name: RUSSIAN CONTROLPOINT CITY
      id: RUSSIAN_CONTROLPOINT_CITY
      position:
        x: '370.00'
        'y': '103.00'
        z: '524.83'
    - name: CONTROLPOINT Kharkov
      id: CONTROLPOINT_Kharkov
      position:
        x: '505.12'
        'y': '119.58'
        z: '416.28'
    - name: AXIS BASE
      id: AXIS_BASE
      position:
        x: '732.92'
        'y': '92.90'
        z: '172.09'
    - name: RUSSIAN BASE
      id: RUSSIAN_BASE
      position:
        x: '221.95'
        'y': '92.89'
        z: '270.45'
  created: '2003-08-25'
  creator: Dnamro
origin: https://www.gamefront.com/games/battlefield-1942/file/gcx-two-moons
---

