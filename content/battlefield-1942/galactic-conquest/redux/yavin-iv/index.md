---
title: Yavin IV
author: Django
description: YavinIV testmap jedi vs Darkjedi
date: '2015-08-23'
categories:
  - Battlefield 1942
  - Redux
tags:
  - Conquest
  - Coop
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: redux
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '539.29'
        'y': '127.00'
        z: '878.82'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '539.54'
        'y': '127.00'
        z: '171.87'
    - name: Forrest
      id: forrest
      position:
        x: '431.19'
        'y': '126.97'
        z: '583.92'
    - name: Forrest 2
      id: forrest2
      position:
        x: '269.75'
        'y': '127.00'
        z: '661.61'
    - name: Forrest 3
      id: forrest3
      position:
        x: '843.79'
        'y': '126.97'
        z: '588.59'
    - name: Forrest 4
      id: forrest4
      position:
        x: '219.72'
        'y': '127.96'
        z: '391.83'
    - name: Forrest 5
      id: forrest5
      position:
        x: '891.10'
        'y': '126.98'
        z: '314.92'
  created: '2015-08-23'
  creator: Django
origin: >-
  https://web.archive.org/save/https://www.lonebullet.com/file/mods/gcx-update-v42c/39587
---

