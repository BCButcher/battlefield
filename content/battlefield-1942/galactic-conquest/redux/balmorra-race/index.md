---
title: Balmorra Race
author: Django
description: ''
date: '2015-08-23'
categories:
  - Battlefield 1942
  - Redux
tags:
  - Conquest
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: redux
  controlpoints:
    - name: Imperial Start
      id: Imperial_Start
      position:
        x: '2084.77'
        'y': '129.14'
        z: '2144.15'
    - name: Rebel Start
      id: Rebel_Start
      position:
        x: '2080.18'
        'y': '132.83'
        z: '2037.60'
  created: '2015-08-23'
  creator: Django
origin: >-
  https://web.archive.org/save/https://www.lonebullet.com/file/mods/gcx-update-v42c/39587
---

