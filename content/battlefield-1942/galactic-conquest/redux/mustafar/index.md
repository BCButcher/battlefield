---
title: Mustafar
author: Django
description: IASP Testmap alpha
date: '2015-09-13'
categories:
  - Battlefield 1942
  - Redux
tags:
  - Conquest
  - Coop
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: redux
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '241.82'
        'y': '139.70'
        z: '196.53'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '809.43'
        'y': '139.70'
        z: '830.31'
    - name: New Control Point 1
      id: New_Control_Point1
      position:
        x: '788.15'
        'y': '139.70'
        z: '324.57'
    - name: New Control Point 2
      id: New_Control_Point2
      position:
        x: '234.18'
        'y': '139.70'
        z: '548.25'
    - name: New Control Point 3
      id: New_Control_Point3
      position:
        x: '524.30'
        'y': '139.67'
        z: '437.26'
  created: '2015-09-13'
  creator: Django
origin: >-
  https://web.archive.org/save/https://www.lonebullet.com/file/mods/gcx-update-v42c/39587
---

