---
title: Balmorra
author: Django
description: Balmorra Test
date: '2015-03-01'
categories:
  - Battlefield 1942
  - Redux
tags:
  - Conquest
  - Coop
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: redux
  controlpoints:
    - name: Bunker CP
      id: Bunker_CP
      position:
        x: '680.61'
        'y': '76.20'
        z: '506.99'
    - name: Factory CP
      id: Factory_CP
      position:
        x: '423.95'
        'y': '78.06'
        z: '629.46'
    - name: Radarstation CP
      id: Radarstation_CP
      position:
        x: '575.82'
        'y': '76.20'
        z: '540.00'
    - name: ATST Factory CP
      id: ATST_Factory_CP
      position:
        x: '434.81'
        'y': '76.20'
        z: '391.77'
    - name: ATAT Factory CP
      id: ATAT_Factory_CP
      position:
        x: '476.27'
        'y': '76.85'
        z: '160.98'
    - name: Laboratory CP
      id: Laboratory_CP
      position:
        x: '363.47'
        'y': '81.98'
        z: '90.07'
    - name: Rebel CP
      id: Rebel_CP
      position:
        x: '659.06'
        'y': '76.80'
        z: '916.47'
  created: '2015-03-01'
  creator: Django
origin: >-
  https://web.archive.org/save/https://www.lonebullet.com/file/mods/gcx-update-v42c/39587
---

