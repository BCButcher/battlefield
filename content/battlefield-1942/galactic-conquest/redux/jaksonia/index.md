---
title: Jaksonia
author: Django
description: Jaksonia, the secret Planet in the Outer Rim
date: '2014-06-20'
categories:
  - Battlefield 1942
  - Redux
tags:
  - Conquest
  - Coop
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: redux
  controlpoints:
    - name: Imperial Base
      id: Imperial_Base
      position:
        x: '228.19'
        'y': '76.18'
        z: '710.01'
    - name: Rebel Base
      id: Rebel_Base
      position:
        x: '360.10'
        'y': '76.19'
        z: '191.69'
    - name: Sand City
      id: Sand_City
      position:
        x: '686.53'
        'y': '86.79'
        z: '802.00'
    - name: Outpost
      id: Outpost
      position:
        x: '828.03'
        'y': '75.90'
        z: '452.01'
    - name: Landing Point
      id: Landing_Point
      position:
        x: '242.06'
        'y': '126.31'
        z: '415.03'
    - name: Arcade Game
      id: Arcade_Game
      position:
        x: '939.94'
        'y': '105.96'
        z: '264.30'
    - name: The City
      id: The_City
      position:
        x: '475.66'
        'y': '83.32'
        z: '541.80'
  created: '2014-06-20'
  creator: Django
origin: >-
  https://web.archive.org/save/https://www.lonebullet.com/file/mods/gcx-update-v42c/39587
---

