---
title: Galactic Conquest Redux
name: Redux
description: >-
  Galactic Conquest Redux is a standalone mod that adds bot support to Galactic
  Conquest, and further improves Galactic Conquest Extended.
categories:
  - Battlefield 1942
  - Galactic Conquest
  - Galactic Conquest Extended
  - Galactic Conquest Redux
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20240109172434/https://www.bfmods.com/viewtopic.php?t=2598
---

