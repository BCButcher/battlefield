---
title: Mini Hoth
author: Kurosaji/Loderunner, GC Team
description: >-
  After the Rebel defeat on Hoth, The Empire has begun developing an
  occupational force on the barren wasteland. The remaining Rebel forces who
  were stranded after the defeat, are taking matters into their own hands.
date: '2015-01-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 8.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Testtemplate
      id: testtemplate
      position:
        x: '464.39'
        'y': '27.91'
        z: '394.01'
    - name: Imperial Strongholdtemplate
      id: imperial_strongholdtemplate
      position:
        x: '475.31'
        'y': '39.79'
        z: '668.83'
    - name: Fallen Walkertemplate
      id: fallen_walkertemplate
      position:
        x: '413.55'
        'y': '39.2714'
        z: '293.92'
    - name: Rebel Strongholdtemplate
      id: rebel_strongholdtemplate
      position:
        x: '329.695'
        'y': '45.57'
        z: '188.259'
    - name: Unnamedcontroltemplate
      id: unnamedcontroltemplate
      position:
        x: '515.242'
        'y': '28.092'
        z: '520.271'
    - name: Peaktemplate
      id: peaktemplate
      position:
        x: '286.304'
        'y': '62.567'
        z: '398.642'
    - name: Armourytemplate
      id: armourytemplate
      position:
        x: '589.809'
        'y': '32.5845'
        z: '420.946'
  created: '2015-01-11'
  creator: Kurosaji/Loderunner, GC Team
origin: https://www.gametracker.com/games/bf1942/forum.php?post=381209
---

