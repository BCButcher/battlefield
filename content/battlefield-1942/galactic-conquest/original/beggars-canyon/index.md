---
title: Beggars Canyon
author: Szier, GC Team
description: ''
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Bridge Passtemplate
      id: bridge_passtemplate
      position:
        x: '1425.66'
        'y': '120.33'
        z: '1009.50'
    - name: Sand Swept Basetemplate
      id: sand_swept_basetemplate
      position:
        x: '1191.83'
        'y': '94.87'
        z: '1038.76'
    - name: Tusken Forttemplate
      id: tusken_forttemplate
      position:
        x: '1536.08'
        'y': '83.15'
        z: '1591.42'
    - name: Toshe Station
      id: toshe_station
      position:
        x: '1588.89'
        'y': '88.50'
        z: '692.05'
    - name: The Citideltemplate
      id: the_citideltemplate
      position:
        x: '638.14'
        'y': '142.00'
        z: '1566.74'
    - name: Rebel Crash Sitetemplate
      id: rebel_crash_sitetemplate
      position:
        x: '1922.70'
        'y': '-16.21'
        z: '149.44'
    - name: Lost Citytemplate
      id: lost_citytemplate
      position:
        x: '1286.43'
        'y': '78.86'
        z: '1610.55'
  created: '2003-03-23'
  creator: Szier, GC Team
origin: https://www.shacknews.com/article/30970/new-galactic-conquest
---

