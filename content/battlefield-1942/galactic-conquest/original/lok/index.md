---
title: Lok
author: GC Team
description: >-
  The forsaken world of Lok will be the Empire's new foothold into the Outer
  Rim. Imperial forces have established an outpost in the southern region of the
  planet. Little do they know, Lok has long been a sympathizer of the Rebellion.
  The notorious smuggler Nym, whose base of operations is located on Lok, has
  informed rebel agents of the situation. A covert group of rebel troops has
  taken control of the Imperial starport in order to cut off Imperial supply
  lines. With the starport secured rebel forces plan to launch an assault on the
  outpost using the Empire's own equipment against them...
date: '2004-09-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 4
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Imperial Headquarters
      id: Imperial_Headquarters
      position:
        x: '1393.12'
        'y': '142.91'
        z: '1586.46'
    - name: Captured Imp Starport
      id: Captured_Imp_Starport
      position:
        x: '1012.89'
        'y': '113.15'
        z: '300.56'
    - name: Research Center
      id: Research_Center
      position:
        x: '814.27'
        'y': '92.50'
        z: '1232.80'
    - name: Junkyard
      id: Junkyard
      position:
        x: '1222.95'
        'y': '90.47'
        z: '1178.44'
    - name: Supply Depot
      id: Supply_Depot
      position:
        x: '971.49'
        'y': '112.29'
        z: '1434.55'
    - name: Communications Center
      id: Communications_Center
      position:
        x: '989.52'
        'y': '129.17'
        z: '1651.43'
  created: '2004-09-11'
  creator: GC Team
origin: https://www.boards.ie/discussion/186606/mod-release-galactic-conquest-0-4
---

