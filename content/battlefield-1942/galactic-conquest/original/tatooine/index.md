---
title: Tatooine
author: Lord_Dill, Django, HC, Szier, Luuri, GC Team
description: >-
  Based on the outter rim of the Galaxy, Tatooine is a very hot and sandy
  planet. Very large open plains are split with huge canyons, usually the
  hunting grounds of many Tusken Raiders. Tatooine is controlled by the Hutts,
  which brings many smugglers, bounty hunters and thieves to the few spaceports
  situated on the planet.


  **Terrain**: Flat desert plains, Dune seas and Huge canyons


  **Inhabitants**: Tusken raiders, Dewbacks, The Hutts, Humans and Jawa's


  **Occupants**: Neutral
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: original
  controlpoints:
    - name: Mos Eisly Starporttemplate
      id: mos_eisly_starporttemplate
      position:
        x: '2901.02'
        'y': '196.751'
        z: '1520.98'
    - name: Cantina
      id: cantina
      position:
        x: '1984.15'
        'y': '107.406'
        z: '139.122'
    - name: The Dune Seatemplate
      id: the_dune_seatemplate
      position:
        x: '620.886'
        'y': '83.3789'
        z: '387.691'
    - name: Jabbas Palacetemplate
      id: jabbas_palacetemplate
      position:
        x: '2148.03'
        'y': '142.386'
        z: '2323.92'
    - name: The Sarlacctemplate
      id: the_sarlacctemplate
      position:
        x: '3379.24'
        'y': '101.462'
        z: '643.653'
    - name: Jawa Camp
      id: jawa_camp
      position:
        x: '1602.72'
        'y': '109.677'
        z: '1211.48'
  created: '2003-03-23'
  creator: Lord_Dill, Django, HC, Szier, Luuri, GC Team
origin: >-
  https://web.archive.org/web/20030829074346/http://www.galactic-conquest.net/index.php?page=&action=show&id=2189
---

