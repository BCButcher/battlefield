---
title: Tatooine Revision 2
author: GC Team
description: ''
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: 2011-mappack
  controlpoints:
    - name: Bestine
      id: Bestine
      position:
        x: '3288.23'
        'y': '188.68'
        z: '2259.62'
    - name: Mos Taike
      id: Mos_Taike
      position:
        x: '696.30'
        'y': '121.41'
        z: '1320.08'
    - name: Skywalker Farm
      id: Skywalker_Farm
      position:
        x: '3722.05'
        'y': '128.27'
        z: '301.59'
    - name: Jabbas Palace
      id: Jabbas_Palace
      position:
        x: '1050.49'
        'y': '139.74'
        z: '2085.52'
    - name: The Sarlacc
      id: The_Sarlacc
      position:
        x: '2124.06'
        'y': '146.47'
        z: '1178.49'
    - name: Jawa Camp
      id: Jawa_Camp
      position:
        x: '2349.94'
        'y': '111.35'
        z: '810.46'
  created: '2011-04-01'
  creator: GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

