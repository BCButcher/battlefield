---
title: Podrace
author: Lord_Dill, Django, JayBiggs, GC Team
description: Let The Pod Race Begin
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: 2011-mappack
  controlpoints:
    - name: Imperial Start
      id: Imperial_Start
      position:
        x: '850.47'
        'y': '195.52'
        z: '265.10'
    - name: Rebel Start
      id: Rebel_Start
      position:
        x: '845.88'
        'y': '195.83'
        z: '158.56'
    - name: Checkpoint
      id: Checkpoint
      position:
        x: '1262.00'
        'y': '245.36'
        z: '1659.34'
    - name: Finish Line
      id: Finish_Line
      position:
        x: '711.90'
        'y': '204.04'
        z: '211.94'
  created: '2011-04-01'
  creator: Lord_Dill, Django, JayBiggs, GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

