---
title: Endor Speederbikes
author: GoosemanXP, GC Team
description: >-
  At the same time the shuttle Tyderium was send to the Forest Moon of Endor, an
  unusual object has been found by (imperial)scout troopers underground close to
  an Ewok village. Immidiatly the Emperor noticed how strong the Force was with
  this object. He ordered his troops to protect the object and complete the
  excavations. The object must then be taken to his pressence as soon as
  possible. Having the potential to multiply his power and assure his control
  over all minds all over the galaxy. These transmissions where intercepted and
  decoded by R2D2 while he was accessing the bunker databases. Within no time a
  2nd lambda was send to Endor with some rebel troops and R2 units with a
  mission : To steal and analyse the Endor object. 
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: 2011-mappack
  controlpoints:
    - name: Base 2
      id: Base2
      position:
        x: '897.79'
        'y': '4.88'
        z: '912.49'
    - name: Base 1
      id: Base1
      position:
        x: '128.75'
        'y': '2.08'
        z: '68.49'
    - name: Ewok Town
      id: Ewok_town
      position:
        x: '456.33'
        'y': '0.90'
        z: '503.77'
    - name: Wasted ATST
      id: Wasted_ATST
      position:
        x: '413.50'
        'y': '1.55'
        z: '802.23'
    - name: Cave
      id: Cave
      position:
        x: '923.03'
        'y': '0.92'
        z: '115.57'
  created: '2011-04-01'
  creator: GoosemanXP, GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

