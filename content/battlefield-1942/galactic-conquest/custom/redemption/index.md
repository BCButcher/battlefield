---
title: Redemption
author: canadiens, Lord_Dill, Django
description: >-
  The Rebel Fleet has been lured into an Imperial trap. As soon as Rebel ships
  arrived above the forest moon of Endor to attack the second Death Star, they
  realized the battlestation's shield was still up. Now, Imperial TIE Fighter
  and TIE Bomber squadrons are assulting the scrambling Rebel fleet.
date: '2007-02-11'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
labels:
  - Release 5.3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: custom
  controlpoints:
    - name: Allied Base
      id: AlliedBase
      position:
        x: '0.00'
        'y': '0.00'
        z: '0.00'
    - name: X Wing Crews
      id: X_Wing_Crews
      position:
        x: '0.00'
        'y': '0.00'
        z: '0.00'
    - name: TIE Bomber Crews
      id: TIE_Bomber_Crews
      position:
        x: '0.00'
        'y': '0.00'
        z: '0.00'
    - name: TIE Interceptor Crews
      id: TIE_Interceptor_Crews
      position:
        x: '0.00'
        'y': '0.00'
        z: '0.00'
    - name: Axis Base
      id: AxisBase
      position:
        x: '3111.25'
        'y': '0.00'
        z: '1018.55'
  created: '2007-02-11'
  creator: canadiens, Lord_Dill, Django
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-redemption.html
---

