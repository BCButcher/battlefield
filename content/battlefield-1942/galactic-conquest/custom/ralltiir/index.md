---
title: Ralltiir
author: Lowie_Smurf
description: >-
  The planet Ralltir has begun to support the growing Rebel Alliance by giving
  them resources. But, when Princess Leia came to visit, she discovered that the
  Empire had begun construction on the Death Star on the same planet! The Battle
  of Ralltir has begun; may the force be with you. game.setServerInfoIcon
  ../../bf1942/levels/GC_Ralltiir/menu/serverInfo.dds game.setMapId 
date: '2004-09-13'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
  - CTF
labels:
  - Release 4
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 2048
  source: custom
  controlpoints: []
  created: '2004-09-13'
  creator: Lowie_Smurf
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/ralltiir.html
---

