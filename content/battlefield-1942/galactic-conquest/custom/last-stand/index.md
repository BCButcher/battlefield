---
title: Last Stand
author: ''
description: ' Yesterday a rebel blockade runner crash landed into the newly discovered planet ''Ha52''. There was a small imperial base there which the surviving rebels quickly captured. Today, the isd Anihalation, is in orbit around the planet. It has landed numerous walkers and infantry. For the rebels on Ha52 this is their last stand'
date: '2004-09-29'
categories:
  - Battlefield 1942
  - Custom
tags:
  - Conquest
  - CTF
  - TDM
labels:
  - Release 3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: custom
  controlpoints:
    - name: Rebel Support Line
      id: Rebel_support_line
      position:
        x: '707.13'
        'y': '87.69'
        z: '103.90'
    - name: 2nd Line
      id: 2nd_line
      position:
        x: '518.48'
        'y': '76.20'
        z: '287.17'
    - name: Imps Start 1
      id: Imps_start_1
      position:
        x: '305.20'
        'y': '76.20'
        z: '845.85'
    - name: Imp Start 2
      id: imp_start_2
      position:
        x: '852.81'
        'y': '76.20'
        z: '944.03'
    - name: Security Checkpoint 1
      id: Security_checkpoint_1
      position:
        x: '409.43'
        'y': '76.46'
        z: '264.38'
    - name: Rebel Control Room
      id: Rebel_Control_room
      position:
        x: '370.07'
        'y': '77.03'
        z: '199.77'
  created: '2004-09-29'
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/gc-last-stand.html
---

