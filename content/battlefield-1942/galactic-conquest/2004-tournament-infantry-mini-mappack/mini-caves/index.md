---
title: Mini Caves
author: GC Team
description: Deep within the caverns of Dagobah, an epic power struggle unfolds...
date: '2004-10-13'
categories:
  - Battlefield 1942
  - 2004 Tournament Infantry Mini Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: 2004-tournament-infantry-mini-mappack
  controlpoints:
    - name: Rebel Base
      id: Rebel_Base
      position:
        x: '427.73'
        'y': '102.20'
        z: '482.25'
    - name: Upper Bridge Rebel Side
      id: Upper_Bridge_Rebel_Side
      position:
        x: '398.13'
        'y': '111.81'
        z: '558.53'
    - name: Lower Bridge Rebel Side
      id: Lower_Bridge_Rebel_Side
      position:
        x: '447.06'
        'y': '83.23'
        z: '591.29'
    - name: Lower Bridge Imperial Side
      id: Lower_Bridge_Imperial_Side
      position:
        x: '449.98'
        'y': '84.08'
        z: '641.15'
    - name: Upper Bridge Imperial Side
      id: Upper_Bridge_Imperial_Side
      position:
        x: '394.25'
        'y': '114.29'
        z: '659.58'
    - name: Imperial Base
      id: Imperial_Base
      position:
        x: '394.41'
        'y': '99.76'
        z: '766.48'
  created: '2004-10-13'
  creator: GC Team
---

