---
title: Jolt Tournament GC Infantry Mappack
name: 2004 Tournament Infantry Mini Mappack
description: >-
  The Official Jolt GC Infantry Tournament is almost upon us! Clans competing in
  the competition can now download the ?GC_Mini? map pack at the mirrors below!
  These maps have been designed as 4v4 player, infantry-only, maps.
categories:
  - Battlefield 1942
  - Galactic Conquest
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20231228001054/https://www.bf-games.net/downloads/308/jolt-tournament-gc-infantry-mappack.html
---

