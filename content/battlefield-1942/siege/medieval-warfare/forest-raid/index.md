---
title: Forest Raid
author: ''
description: Let's fight in the forest!
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: medieval-warfare
  controlpoints:
    - name: Barn
      id: Barn
      position:
        x: '360.03'
        'y': '127.00'
        z: '376.00'
    - name: Crossroad
      id: Crossroad
      position:
        x: '512.64'
        'y': '126.99'
        z: '541.13'
    - name: Stones
      id: Stones
      position:
        x: '631.00'
        'y': '127.00'
        z: '660.00'
---

