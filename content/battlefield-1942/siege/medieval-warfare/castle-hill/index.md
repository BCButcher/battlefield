---
title: Castle Hill
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Medieval Warfare
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: medieval-warfare
  controlpoints: []
---

