---
title: Monte Cassino
author: DICE, EA
description: ''
date: '2003-02-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base 1 Cpoint
      id: AxisBase_1_Cpoint
      position:
        x: '519.669'
        'y': '167.823'
        z: '323.103'
    - name: Allies Base 2 Cpoint
      id: AlliesBase_2_Cpoint
      position:
        x: '617.287'
        'y': '80.1398'
        z: '894.636 '
    - name: Hillbase 1 Cpoint
      id: hillbase_1_Cpoint
      position:
        x: '538.008'
        'y': '99.3498'
        z: '679.906'
    - name: Hillbase 2 Cpoint
      id: hillbase_2_Cpoint
      position:
        x: '572.746'
        'y': '119.421'
        z: '571.097'
    - name: Hillbase 3 Cpoint
      id: hillbase_3_Cpoint
      position:
        x: '532.042'
        'y': '139.463'
        z: '490.102'
    - name: Cassinobase 1 Cpoint
      id: cassinobase_1_Cpoint
      position:
        x: '536.847'
        'y': '162.104'
        z: '414.706'
  created: '2003-02-02'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

