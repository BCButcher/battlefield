---
title: Santo Croce
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: ALLIESBASE
      id: ALLIESBASE
      position:
        x: '1109.67'
        'y': '25.2'
        z: '1420.53'
    - name: AXISBASE
      id: AXISBASE
      position:
        x: '876.79'
        'y': '25.2'
        z: '1440.63'
    - name: AXISBASE Supply
      id: AXISBASE_supply
      position:
        x: '854.23'
        'y': '14.9578'
        z: '962.886'
    - name: ALLIES Base Supply
      id: ALLIESBase_supply
      position:
        x: '1148.29'
        'y': '12.5996'
        z: '964.855'
    - name: Village Allies
      id: Village_allies
      position:
        x: '1075.91'
        'y': '26.8144'
        z: '982.22'
    - name: Village Axis
      id: village_axis
      position:
        x: '933.325'
        'y': '31.4413'
        z: '975.137'
    - name: Top Allies
      id: top_allies
      position:
        x: '1101.14'
        'y': '62.2805'
        z: '793.065'
    - name: Top Axis
      id: top_axis
      position:
        x: '914.047'
        'y': '62.168'
        z: '799.332'
    - name: Top
      id: top
      position:
        x: '1001.68'
        'y': '76.7344'
        z: '874.033'
---

