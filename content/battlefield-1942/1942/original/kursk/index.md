---
title: Kursk
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base 1 Cpoint
      id: AxisBase_1_Cpoint
      position:
        x: '437.315'
        'y': '77.8547'
        z: '238.39'
    - name: Allies Base 2 Cpoint
      id: AlliesBase_2_Cpoint
      position:
        x: ' 568.058'
        'y': '76.6406'
        z: '849.956'
    - name: Openbase Lumbermill Cpoint
      id: openbase_lumbermill_Cpoint
      position:
        x: '639.658'
        'y': '83.3344'
        z: '556.038'
    - name: Openbasecammo
      id: 'openbasecammo '
      position:
        x: '508.833'
        'y': '84.8718'
        z: '582.2'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

