---
title: Bocage
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: AXISBASE Cpoint
      id: 'AXISBASE_Cpoint '
      position:
        x: '590'
        'y': '33.9902'
        z: '1078'
    - name: ALLIES Base Cpoint
      id: 'ALLIESBase_Cpoint '
      position:
        x: '1214'
        'y': '20.6109'
        z: '566'
    - name: Broaxis Cpoint
      id: 'broaxis_Cpoint '
      position:
        x: '760'
        'y': '30.9'
        z: '940.5'
    - name: Bro Alleis Cpoint
      id: 'broAlleis_Cpoint '
      position:
        x: '1019.5'
        'y': '21.9103'
        z: '807.5'
    - name: Lumbermill Cpoint
      id: 'Lumbermill_Cpoint '
      position:
        x: '838'
        'y': '33.1'
        z: '722'
    - name: City Cpoint
      id: 'city_Cpoint '
      position:
        x: '711.5'
        'y': '33.7069'
        z: '1095.5'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

