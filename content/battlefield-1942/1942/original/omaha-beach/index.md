---
title: Omaha Beach
author: ''
description: ''
date: ''
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: The Beach
      id: the_beach
      position:
        x: '976.313'
        'y': '18.406'
        z: '1018.35'
    - name: The Wall
      id: the_wall
      position:
        x: '982.667'
        'y': '44.7988'
        z: '1149.03'
    - name: The City Etry
      id: the_City_etry
      position:
        x: '1068.53'
        'y': '61.1277'
        z: '1291.24'
    - name: The Axisbase
      id: the_axisbase
      position:
        x: '1006.02'
        'y': '63.3815'
        z: '1297.05'
---

