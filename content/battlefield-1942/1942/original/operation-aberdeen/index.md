---
title: Operation Aberdeen
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '78.5131'
        'y': '92.8735'
        z: '125.406'
    - name: Allies Base
      id: AlliesBase
      position:
        x: '897.278'
        'y': '93.6854'
        z: '913.886'
    - name: South Desert
      id: South_Desert
      position:
        x: '651.878'
        'y': '76.7171'
        z: '225.374'
    - name: Openbase Watersupply
      id: 'openbase_watersupply '
      position:
        x: '384.16'
        'y': '59.0148'
        z: '433.31'
    - name: North Desert
      id: north_Desert
      position:
        x: '400.405'
        'y': '75.3131'
        z: '768.854'
    - name: East Village
      id: East_Village
      position:
        x: '647.073'
        'y': '58.3561'
        z: '558.065'
    - name: City
      id: City
      position:
        x: '526.524'
        'y': '65.4817'
        z: '471.795'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

