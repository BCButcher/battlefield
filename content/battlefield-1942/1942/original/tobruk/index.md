---
title: Tobruk
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - TDM
  size: 4096
  source: original
  controlpoints:
    - name: ALLIES BASE
      id: 'ALLIES_BASE '
      position:
        x: '2546.28'
        'y': '69.6516'
        z: '817.944'
    - name: ALLIES BASE CITY
      id: ALLIES_BASE_CITY
      position:
        x: '2401.45'
        'y': '80.6862'
        z: '819.998'
    - name: ALLIES BASE SECONDLINE Right
      id: 'ALLIES_BASE_SECONDLINE_right '
      position:
        x: '2233.68'
        'y': '73.7306'
        z: '717.589'
    - name: ALLIES BASE SECONDLINE Left
      id: 'ALLIES_BASE_SECONDLINE_left '
      position:
        x: '2217.26'
        'y': '78.6635'
        z: '842.967'
    - name: ALLIES BASE FIRSTLINE Right
      id: 'ALLIES_BASE_FIRSTLINE_right '
      position:
        x: '2083.9'
        'y': '69.2736'
        z: '612.068'
    - name: ALLIES BASE FIRSTLINE Middle
      id: 'ALLIES_BASE_FIRSTLINE_middle '
      position:
        x: '2038.04'
        'y': '71.5807'
        z: '724.311'
    - name: ALLIES BASE FIRSTLINE Left
      id: ALLIES_BASE_FIRSTLINE_left
      position:
        x: '1976.1'
        'y': '80.5135'
        z: '841.519'
    - name: AXIS BASE
      id: AXIS_BASE
      position:
        x: '1743.65'
        'y': '67.1438'
        z: '572.867'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

