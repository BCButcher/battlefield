---
title: Gazala
author: DICE, EA
description: ''
date: '2002-09-10'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: AXIS BASE
      id: 'AXIS_BASE '
      position:
        x: '643.045'
        'y': '48.7724'
        z: '674.484'
    - name: ALLIES BASE
      id: ALLIES_BASE
      position:
        x: '1490.96'
        'y': '42.7617'
        z: '1550.85'
    - name: ALLIES Village
      id: 'ALLIES_village '
      position:
        x: '1642.8'
        'y': '73.4602'
        z: '603.905'
    - name: AXIS Village
      id: 'AXIS_village '
      position:
        x: '1268.79'
        'y': '73.4546'
        z: '286.608'
    - name: 0 PEN BASE ROAD
      id: '0PEN_BASE_ROAD '
      position:
        x: '1392.13'
        'y': '66.7804'
        z: '853.743'
    - name: 0 PEN BASE AIRFIELD
      id: '0PEN_BASE_AIRFIELD '
      position:
        x: '402.496'
        'y': '44.4469'
        z: '1855.9'
    - name: 0 PEN BASE Crossing
      id: 0PEN_BASE_crossing
      position:
        x: '1568.71'
        'y': '80.8617'
        z: '312.324'
  created: '2002-09-10'
  creator: DICE, EA
origin: https://academickids.com/encyclopedia/index.php/Battlefield_1942
---

