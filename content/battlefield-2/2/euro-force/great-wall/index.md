---
title: Great Wall
author: DICE, EA
description: >-
  A newly negotiated peace with Russia has allowed the European Union to launch
  an attack into mainland China from the north. The EU forces hope to breach the
  Great Wall of China and establish a base for future operations before
  continuing south to the coast, but their supply lines are dangerously thin. If
  the Chinese forces can head off the assault and hold them back long enough,
  the EU will have no choice but to retreat back into Russia. It's vital for
  them to link up with American forces attacking from the coast, or a sustained
  assault on China will be impossible!
date: '2005-06-21'
categories:
  - Battlefield 2
  - Euro Force
tags:
  - Conquest
labels:
  - Euro Force
map:
  gametypes:
    - Conquest
  sizes: []
  source: euro-force
  controlpoints: []
  created: '2005-06-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Great_Wall
---

