---
title: Operation Smoke Screen
author: DICE, EA
description: >-
  War continues to rage for the precious oil in the Middle East. The EU has come
  to aid their allies, confronting the MEC head-on in one of the most brutal
  battles of the war. Multiple assaults on both fronts have pushed the armies
  back to their bases, decimating the middle ground and leaving the oil field a
  smoking ruin. Even though the oil reserves have been destroyed, what remains
  beneath the scorched desert sand makes this a battleground worth fighting for.
date: '2005-06-21'
categories:
  - Battlefield 2
  - Euro Force
tags:
  - Conquest
labels:
  - Armored Fury
map:
  gametypes:
    - Conquest
  sizes: []
  source: euro-force
  controlpoints: []
  created: '2005-06-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Operation_Smoke_Screen
---

