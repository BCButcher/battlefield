---
title: 'Battlefield 2: Euro Force'
name: Euro Force
description: >-
  Battlefield 2: Euro Force is the first booster pack. The booster pack allows
  players to play as a new European Union army, armed with new weapons and
  vehicles from the various countries of the EU.
categories:
  - Battlefield 2
type: source
layout: layouts/source.njk
origin: https://en.wikipedia.org/wiki/Battlefield_2#Euro_Force
banner: true
---

