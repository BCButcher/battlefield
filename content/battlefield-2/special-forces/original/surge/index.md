---
title: Surge
author: DICE, EA
description: >-
  Local Rebels have reactivated an abandoned missile launch site in Kazakhstan
  and plan to launch a missile they have managed to piece together through
  acquisitions on the black market. The Spetsnaz are moving in to sabotage the
  launch site and clear out the insurgency.
date: '2005-11-21'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels: []
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: '2005-11-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Surge
---

