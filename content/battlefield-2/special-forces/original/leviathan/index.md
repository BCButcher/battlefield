---
title: Leviathan
author: DICE, EA
description: >-
  American nuclear submarines are lying peacefully in their pens, undergoing
  repairs at a naval base in the Persian Gulf. Unknown to the elite Navy SEALs
  guarding the facility, MEC Special Forces are about to begin a night-time raid
  to destroy the submarines. Should they succeed, the nuclear catastrophe could
  mark the beginning of the end for US forces in the Middle East.
date: '2005-11-21'
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels: []
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: '2005-11-21'
  creator: DICE, EA
type: map
layout: layouts/map.njk
origin: https://battlefield.fandom.com/wiki/Leviathan
---

