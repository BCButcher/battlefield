---
title: 'Battlefield 2: Special Forces'
name: Special Forces
description: >-
  The expansion pack provides eight maps, 6 playable factions, and ten more
  vehicles such as the AH-64D Apache and Mi-35 Hind, though all jets have been
  removed. In addition to these new contents, players have access to new
  equipment such as night vision goggles, tear gas, gas masks, zip lines and
  grappling hooks which can alter gameplay.
categories:
  - Battlefield 2
type: mod
layout: layouts/mod.njk
origin: https://en.wikipedia.org/wiki/Battlefield_2#Battlefield_2:_Special_Forces
banner: true
---

