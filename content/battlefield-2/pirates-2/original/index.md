---
title: Battlefield Pirates 2 Original
name: Original
description: >-
  Battlefield Pirates 2 was released as an open beta on September 20th 2007.
  Release 1 showcased the many weapons and kits available thru a series of
  infantry based maps. Release 2, from December 18th 2008, provided a deeper
  experience with more naval combat maps, new ships, and even aircraft. It also
  included Capture The Flag and Zombie modes.
categories:
  - Pirates
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20090126005944/http://bfpirates.com:80/wiki/index.php/BFP2
---

