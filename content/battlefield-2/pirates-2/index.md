---
title: Pirates 2
name: Pirates 2
description: >-
  Battlefield Pirates 2 is a full conversion mod for Battlefield 2. The game
  pits 2 two rivaling crews, the Undead and the Peglegs, in a vicious battle for
  control of the Caribbean and all its booty. Brace yourself for a fun-filled
  adventure on the high seas!
categories:
  - Pirates
type: mod
layout: layouts/mod.njk
banner: true
---

