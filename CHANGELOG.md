# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.2] - 2024-05-01

### Fixed

- Rebuild

## [3.1.1] - 2024-05-01

### Changed

- Allow nested structures in Gallery

### Fixed

- Social Sharing Image

## [3.1.0] - 2024-04-29

### Added

- Missing extract-scripts
- BF2-sources

### Fixed

- Workaround for large compressed files (>100MB)

## [3.0.0] - 2024-04-13

### Changed

- Eleventy as build-system
- Unified and normalized all paths

### Added

- Optimize images on build
- Generate and apply social-sharing (OpenGraph) images
- Social-sharing image preview in footer

### Removed

- Previous git history

### Fixed

- Many parts of `extract` and `generate`

## [2.0.0] - 2022-07-30

### Changed

- Last release with Hugo

## [1.1.1] - 2022-07-21

### Changed

- Optimized image zoom

## [1.1.0] - 2022-06-22

### Changed

- Optimized images
- Adjustments to build-system

### Fixed

- Many parts of `extract` and `generate`

## [1.0.2] - 2022-06-11

### Changed

- Adjustments to build-system

## [1.0.2] - 2022-06-11

### Added

- DC mod

## [1.0.1] - 2022-06-06

### Added

- Galactic Conquest mod
- Siege mod

### Changed

- Adjustments to build-system

## [1.0.0] - 2022-05-31

### Changed

- Orphan release
- First release with Hugo as build-system
