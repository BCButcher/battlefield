const FS = require("fs"),
  child_process = require("child_process"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  walker = require("klaw-sync"),
  sizeOf = require("image-size"),
  byteSize = require("byte-size"),
  chalk = require("chalk"),
  { getArgs, msToTime, percentageChange } = require("./utilities");
const args = getArgs();
const start = performance.now();
const MAGICK = path.resolve(
  "./bin/imagemagick-7.1.1-27-portable-Q16-x64/convert.exe",
);

console.log(chalk.magenta("OPTIMIZING IMAGES ..."));

let base;
if (args.base) {
  base = path.resolve(args.base).replace(/\\/g, "/");
} else {
  return;
}
if (args.debug) console.log(chalk.cyanBright("BASE"), base);
let targetFolder = "/_site/";
if (args.target) {
  targetFolder = `/_site/${args.target}`;
} else {
  return;
}
if (args.debug) console.log(chalk.cyanBright("TARGET FOLDER"), targetFolder);

const htmlFiles = walker(`${base}/_site`, {
  fs: FS,
  nodir: true,
  depthLimit: -1,
  traverseAll: true,
  filter: (item) => {
    return [".html"].includes(path.extname(item.path));
  },
});
if (args.debug)
  console.log(
    chalk.cyanBright("FOUND"),
    htmlFiles.length,
    "HTML file(s) in BASE/_site",
  );

const imageFiles = walker(`${base}${targetFolder}`, {
  fs: FS,
  nodir: true,
  depthLimit: -1,
  traverseAll: true,
  filter: (item) => {
    return [".jpg", ".jpeg", ".png", ".webp"].includes(path.extname(item.path));
  },
});
let images = [];
let imagesSize = 0;
for (let i = 0; i < imageFiles.length; i++) {
  const extension = path.extname(imageFiles[i].path);
  const size = FS.statSync(imageFiles[i].path).size;
  const dimensions = sizeOf(imageFiles[i].path);
  let longestSide = dimensions.width;
  if (dimensions.height > dimensions.width) {
    longestSide = dimensions.height;
  }
  if (longestSide > 1920 || size > 600000) {
    let filepath = imageFiles[i].path.replace(/\\/g, "/");
    images.push(filepath);
    imagesSize = imagesSize + FS.statSync(filepath).size;
  }
}
if (args.debug)
  console.log(
    chalk.cyanBright("FOUND"),
    images.length,
    "image(s) to optimize in BASE",
    targetFolder,
  );

let optimizedImagesSize = 0;
for (let i = 0; i < images.length; i++) {
  const target = images[i].replace(path.extname(images[i]), ".webp");
  child_process
    .execSync(`${MAGICK} "${images[i]}" -resize "1920>" "${target}"`, {
      timeout: 10000,
      stdio: "pipe",
    })
    .toString();
  optimizedImagesSize = optimizedImagesSize + FS.statSync(target).size;
  if (args.debug)
    console.log(chalk.cyanBright("WROTE"), target.replace(base, ""));
  if (args.remove) FS.rmSync(images[i]);
}

let changes = 0;
for (let n = 0; n < htmlFiles.length; n++) {
  let fileContent = FS.readFileSync(htmlFiles[n].path, "utf8");
  for (let i = 0; i < images.length; i++) {
    const original = images[i].replace(base, "").replace("/_site/", "");
    const changed = images[i]
      .replace(base, "")
      .replace(path.extname(images[i]), ".webp")
      .replace("/_site/", "");
    fileContent = fileContent.replaceAll(original, changed);
    if (args.debug)
      console.log(
        chalk.magenta("REPLACED"),
        chalk.yellowBright(original),
        "=>",
        chalk.greenBright(changed),
      );

    const regexChanged = new RegExp(`\\b${changed}\\b`, "gi");
    let matchesChanged = fileContent.match(regexChanged);
    if (matchesChanged) {
      changes = changes + matchesChanged.length;
    }
  }
  if (args.write) FS.writeFileSync(htmlFiles[n].path, fileContent);
}

console.log(
  chalk.magenta("OPTIMIZED"),
  chalk.whiteBright(images.length),
  "image(s) as WebP -",
  chalk.yellowBright(`${byteSize(imagesSize)}`),
  "=>",
  chalk.greenBright(`${byteSize(optimizedImagesSize)}`),
  `(${percentageChange(imagesSize, optimizedImagesSize, 2)}%),`,
  chalk.whiteBright(changes),
  "changes to HTML file(s) in",
  chalk.whiteBright(msToTime(performance.now() - start)),
);
