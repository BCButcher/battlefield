function debounce(fn, threshold) {
  var timeout;
  threshold = threshold || 100;
  return function debounced() {
    clearTimeout(timeout);
    var args = arguments;
    var _this = this;
    function delayed() {
      fn.apply(_this, args);
    }
    timeout = setTimeout(delayed, threshold);
  };
}

function generateTableOfContents(elements, target) {
  if (elements.length <= 1) {
    return;
  }
  var text, href, currentLevel;
  var prevLevel = 0;
  var container = document.querySelector(`${target} > div`);
  for (var i = 0; i < elements.length; i++) {
    text = window.headingAnchors.elements[i].textContent;
    if (
      !window.headingAnchors.elements[i].querySelector(".anchorjs-link") ||
      !window.headingAnchors.elements[i]
        .querySelector(".anchorjs-link")
        .hasAttribute("href")
    ) {
      return;
    }
    href = window.headingAnchors.elements[i]
      .querySelector(".anchorjs-link")
      .getAttribute("href");
    currentLevel = anchorLevel(elements[i].nodeName);
    container = navTreeNode(container, currentLevel - prevLevel);
    addNavItem(container, href, text);
    prevLevel = currentLevel;
  }
  document.querySelector(target).style.display = "block";
}
function addNavItem(ol, href, text) {
  var listItem = document.createElement("li"),
    anchorItem = document.createElement("a"),
    textNode = document.createTextNode(text);
  anchorItem.href = href;
  ol.appendChild(listItem);
  listItem.appendChild(anchorItem);
  anchorItem.appendChild(textNode);
}
function anchorLevel(nodeName) {
  return parseInt(nodeName.charAt(1) - 1);
}
function navTreeNode(current, moveLevels) {
  var e = current;
  if (moveLevels > 0) {
    var ol;
    for (var i = 0; i < moveLevels; i++) {
      ol = document.createElement("ol");
      e.appendChild(ol);
      e = ol;
    }
  } else {
    for (var i = 0; i > moveLevels; i--) {
      e = e.parentElement;
    }
  }
  return e;
}

if (document.querySelector("#sun")) {
  document.querySelector("#sun").addEventListener("click", function (event) {
    if (document.querySelector("html").dataset.theme == "light") {
      document.querySelector("html").dataset.theme = "dark";
    } else if (document.querySelector("html").dataset.theme == "dark") {
      document.querySelector("html").dataset.theme = "light";
    } else {
      document.querySelector("html").dataset.theme = "dark";
    }
  });
}
